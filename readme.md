Inside these folders are short snippets of code, related or otherwise, that demonstrate some mathematical result but are not important enough (in my opinion) to warrant their own repository. Most of these files are written in Python, my main programming language.

The name comes from the Dounreay Nuclear Power Development Establishment in the far north of Scotland, which operated until 1994.
