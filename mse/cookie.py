#!/usr/bin/env python3
# https://math.stackexchange.com/q/2000972
from fractions import Fraction as F

E = [0]
while len(E) <= 10:
    n = len(E)
    E.append(F(2 ** n, 2 ** n - 1) * (1 + sum([E[k] * F(comb(n, k), 2 ** n) for k in range(n)])))
print(E[10], float(E[10])) # 1777792792/376207909 4.725559323634528
