#!/usr/bin/env python3
# https://math.stackexchange.com/q/1977081
import re
from collections import Counter
dnd = re.compile("10*")

# Prepare counts of ordered partitions of 10 to 14 into parts of size at most 5.
counts = [[], [], [], [], []]
for p in range(5):
    for N in range(2 ** (p + 9), 2 ** (p + 10)):
        rolls = [len(m.group()) for m in dnd.finditer("{:b}".format(N))]
        if max(rolls) <= 5: counts[p].append(len(rolls))
    counts[p] = Counter(counts[p])
    print("\t".join(str(counts[p][np]) for np in range(2, 15)))

# Work out the expected score for the "gambling on the six" game using a stop-at-15 strategy.
S = [0, 0, 0, 0, 0]
for p in range(5):
    for pre in range(2, 15): S[p] += counts[p][pre] * 6 ** (14 - pre)
    S[p] *= sum(range(15, 16 + p))
print(sum(S) / 6 ** 15) # 2893395172951 / 470184984576 = 6.1537379284...
