#!/usr/bin/env python3
# https://math.stackexchange.com/q/2137530
from sympy.functions.combinatorial.factorials import binomial
from sympy import Symbol, pprint, sqrt, oo
from sympy.series.limits import limit
n = Symbol('n', integer=True, positive=True)

def kstirling(n, k): # binom(6, k) k! S(n, k)
    return binomial(6, k) * sum([(-1) ** (k - j) * binomial(k, j) * j ** n for j in range(k + 1)])

d = [kstirling(n, q) / 6 ** n for q in range(1, 7)] # the probabilities
print(sum(d).simplify()) # 1; this confirms that d is a distribution
# Expected value (mean)
m = sum([(i + 1) * d[i] for i in range(6)]).simplify()
print(m) # 6(1 - (5/6)^n)
print(limit(m, n, oo)) # 6
# Variance
v = sum([d[i] * (i + 1 - m) ** 2 for i in range(6)]).simplify()
print(v) # (5*144^n - 6*150^n + 180^n) / (6^(3n-1))
print(limit(v, n, oo)) # 0
