#!/usr/bin/env python3.6
import sys
from mpmath import *
mp.dps = 100

def vadd(a, b, c): # vector addition; the returned point d is such that abdc is a parallelogram
    return [b[i] + c[i] - a[i] for i in (0, 1)]
def reflect(a, b): # reflects b across the line through origin and a
    common = 2 * a[0] / (a[0] ** 2 + a[1] ** 2)
    s2t = common * a[1]
    c2t = common * a[0] - 1
    return [c2t * b[0] + s2t * b[1], s2t * b[0] - c2t * b[1]]
def isosceles_on(a, b, r): # point at distance r from both a and b, with that point on the right of b as seen from a
    k = sqrt(r ** 2 / ((b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2) - 0.25)
    return [(a[0] + b[0]) / 2 + (b[1] - a[1]) * k, (a[1] + b[1]) / 2 + (a[0] - b[0]) * k]
def cosine_rule(a, r): # constructs the point on the unit circle with positive y-coordinate and distance r from [a, 0]
    x = (1 + a ** 2 - r ** 2) / (2 * a)
    return [x, sqrt(1 - x ** 2)]
def distance_condition(a, b, r): # condition expressing that the distance between a and b must be r
    return (a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2 - r ** 2
def circle_intersect(c, r, d, s):
    s2, r2 = s ** 2, r ** 2
    c_1, c_2 = c
    d_1, d_2 = d
    dcd1, dcd2 = c_1 - d_1, c_2 - d_2
    e0 = dcd1 ** 2 + dcd2 ** 2
    e1 = (c_2 + d_2) * e0 + (s2 - r2) * dcd2
    e2 = sqrt( ((r + s) ** 2 - e0) * (e0 - (r - s) ** 2) ) * dcd1
    e3 = (c_1 ** 2 + c_2 ** 2 - (d_1 ** 2 + d_2 ** 2) - r2 + s2) * e0
    s1 = [(e3 - dcd2 * (e1 + e2)) / (2 * dcd1 * e0), (e1 + e2) / (2 * e0)]
    s2 = [(e3 - dcd2 * (e1 - e2)) / (2 * dcd1 * e0), (e1 - e2) / (2 * e0)]
    return [s1, s2]
def radiustranslate(a, b, c, r, s):
    # the vector from b to c can be translated such that b is on the circle and len(ac) = r;
    # s determines the orientation; returns where c ends up
    return circle_intersect(a, r, [c[0] - b[0], c[1] - b[1]], 1)[s]

def filterrows(M, row_indices):
    """Removes the rows of M with the given row_indices."""
    return matrix([M[row,:].tolist()[0] for row in range(M.rows) if row not in row_indices])

def sk_g(r, v_cart, symmetry):
    """Generates Szabó–Kollár G in polar coordinates. Identifies which points are on the boundary by the symmetry group:
    1, 2, 3… indicate D_1, D_2, D_3…
    -1, -2, -3 indicate C_1, C_2, C_3…"""
    v_polar, boundaries = [], []
    nverts = v_cart.rows
    for v in range(nverts):
        x, y = v_cart[v,:]
        rd, theta = polar(mpc(x, y))
        v_polar.append([rd, theta])
        if abs(rd - 1) < 1e-5: boundaries.append(2 * v)
        if symmetry > 0 and (abs(theta) < 1e-12 or abs(abs(theta) - pi / symmetry) < 1e-12): boundaries.append(2 * v + 1)
    edges = [(i, j) for i in range(nverts) for j in range(i) if abs(norm(v_cart[i,:] - v_cart[j,:]) / r - 1) <= 1e-3]
    nedges = len(edges)
    
    def barcontrib(i, j): # Contribution of the stress on i from j to the matrix
        denom, r0v, rv = v_polar[i][0] * r, v_cart[i,:], v_cart[j,:] - v_cart[i,:]
        if denom == 0: return matrix([1, 0])
        return matrix([r0v[0] * rv[0] + r0v[1] * rv[1], r0v[0] * rv[1] - r0v[1] * rv[0]]) / denom
    
    GT = matrix(2 * nverts, nedges) 
    for col in range(nedges):
        i, j = edges[col]
        GT[2 * i:2 * (i + 1),col] = barcontrib(i, j)
        GT[2 * j:2 * (j + 1),col] = barcontrib(j, i)
    return edges, filterrows(GT, boundaries).T

constructions = {'V': "vadd", 'R': "reflect", 'I': "isosceles_on"}
def parse_code():
    with open(sys.argv[1], 'r') as f: cmds = f.readlines()
    symmetry, *params = cmds[0].split()
    symmetry = (-1 if symmetry[0] == 'C' else 1) * int(symmetry[1:])
    params = [[param.partition('=')[0], float(param.partition('=')[2])] for param in params]
    body = f'def sk_r({", ".join([param[0] for param in params])}):\n    p = {{}}\n'
    for cmd in cmds[1:-1]:
        index, rest = cmd.split(maxsplit=1)
        line = f"    p[{index}] = "
        if '[' in rest: line += f"{rest[:-1]}\n"
        else:
            op, rest = rest.split(maxsplit=1)
            if op == 'C': line += f"cosine_rule({rest[:-1]}, r)\n"
            elif op == 'T':
                a, b, c, s = rest.split()
                line += f"radiustranslate(p[{a}], p[{b}], p[{c}], r, {s})\n"
            else:
                points = rest.split()
                line += f"{constructions[op]}({', '.join(f'p[{arg}]' for arg in points)}{', r' if op == 'I' else ''})\n"
        body += line
    body += f"    func = distance_condition({', '.join(f'p[{arg}]' for arg in cmds[-1].split())}, r)\n"
    body += f"    v_cart = matrix([p[i] for i in sorted(list(p))])\n"
    body += f"    return v_cart, func"
    param_names, x0 = [param[0] for param in params], matrix([param[1] for param in params])
    
    exec(body, globals())
    return symmetry, param_names, x0

@extradps(20)
def mulminnewton(f, x0):
    # Minimises the first element of x0 subject to f(*x0) = 0 using the multivariate Newton's method.
    # The scheme is based on Lagrange multipliers; returns the vector of parameters resulting in the minimum.
    n = len(x0)
    x = x0.copy()
    delta = matrix([1 for q in range(n)])
    eps = mpf(f"1e-{mp.dps - 15}")
    while norm(delta) > eps:
        J = zeros(n)
        for i in range(n):
            for j in range(n):
                if 1 <= j < i: J[i,j] = J[j,i]
                D = [0 for i in range(n)]
                if i > 0: D[i] += 1
                D[j] += 1
                J[i,j] = diff(f, x, D)
        b = J[0,:].T
        b[0] = f(*x)
        delta = lu_solve(J, -b)
        x += delta
    return x

def drawcircles(r, v_cart, edges, symmetry):
    """Draws a diagram of the covering based on the point list, edges and symmetry group."""
    # Find circle centre indices using breadth-first search
    nverts, depth, edgeset = v_cart.rows, 0, {frozenset(edge) for edge in edges}
    processed = {i for i in range(nverts) if abs(norm(v_cart[i,:]) - 1) < 1e-5} # boundary vertices
    unprocessed, centres = set().union(*edgeset) - processed, set()
    while unprocessed:
        depth += 1
        nextlayer = set()
        for v in unprocessed:
            edgeremoved = False
            for w in processed:
                pedge = frozenset([v, w])
                if pedge in edgeset:
                    edgeset.remove(pedge)
                    edgeremoved = True
            if edgeremoved: nextlayer.add(v)
        if depth & 1: centres |= nextlayer
        processed, unprocessed = nextlayer, unprocessed - nextlayer
    # Construct actual circle vertices based on given symmetry group
    circles, order = [], abs(symmetry)
    sth, cth = sin(2 * pi / order), cos(2 * pi / order)
    rm = matrix([[cth, sth], [-sth, cth]])
    for centre in centres:
        point = v_cart[centre,:]
        rotations = [point * rm ** n for n in range(order)]
        circles.extend(rotations)
        if symmetry > 0:
            theta = atan2(point[1], point[0])
            if not (abs(theta) < 1e-12 or abs(abs(theta) - pi / order) < 1e-12):
                circles.extend([matrix([[rotation[0], -rotation[1]]]) for rotation in rotations])
    # Draw SVG
    out = '''<svg xmlns="http://www.w3.org/2000/svg" width="1020" height="1020" viewBox="-510 -510 1020 1020">
  <circle r="500" fill="none" stroke="#c0c0c0" stroke-width="4"/>\n'''
    r_ = r * 500
    with workdps(11):
        for c in circles:
            cx, cy = c[0] * 500, -c[1] * 500
            if abs(cx) < 1e-12: cx = 0
            if abs(cy) < 1e-12: cy = 0
            out += f'  <circle cx="{cx}" cy="{cy}" r="{r_}" fill="none" stroke="#000" stroke-width="4"/>\n'
    out += "</svg>"
    with open("out.svg", 'w') as f: f.write(out)

def checktension():
    symmetry, param_names, x0 = parse_code()
    x = mulminnewton(lambda *v: sk_r(*v)[1], x0)
    r = x[0]
    coords = sk_r(*x)[0]
    edges, G = sk_g(r, coords, symmetry)
    print(edges)
    nedges = len(edges)
    G.rows = G.cols
    R = qr(G)[1]
    i, j, nonpivots = 0, 0, []
    while j < R.cols:
        if abs(R[i,j]) < mpf(f"1e-{mp.dps // 2}"):
            nonpivots.append(j)
            if R[i,j] == 0: i -= 1
        i += 1
        j += 1
    G_TILDE = filterrows(G[:nedges,:].T, nonpivots).T
    #if G_TILDE.rows == G_TILDE.cols: raise SyntaxError("no rank drop")
    A = (G_TILDE * (G_TILDE.T * G_TILDE) ** -1 * G_TILDE.T - eye(nedges)) * ones(nedges, 1)
    # If any elements of A are positive, the edges corresponding to those elements are in compression and should be removed.
    # The non-zero bound is to suppress reporting on edges with compression only due to rounding error.
    compressed = [edges[i] for i in range(nedges) if A[i] > mpf(f"1e-{mp.dps // 2}")]
    nprint(A.T, 2)
    if not compressed: print("Covering is locally optimal")
    else: print(f"Edges {compressed} are in compression")
    for l, n in zip(param_names, x): print(l, '=', n)
    print("R =", 1/r)
    drawcircles(r, coords, edges, symmetry)

checktension()
