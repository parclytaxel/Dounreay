#!/usr/bin/env python3.6
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import cKDTree, Delaunay, ConvexHull

def drawpoints(cand):
    r, points = cand
    fig, ax = plt.subplots()
    large_disc = plt.Circle((0, 0), 1, fill=False)
    ax.add_artist(large_disc)
    for point in points:
        small_disc = plt.Circle(point, r, fill=False)
        ax.add_artist(small_disc)
    tri = Delaunay(points)
    plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
    
    for j, p in enumerate(points):
        plt.text(p[0]-0.03, p[1]+0.03, j, ha='right')
    
    plt.xlim(-1.1, 1.1)
    plt.ylim(-1.1, 1.1)
    plt.gca().set_aspect('equal', adjustable='box')
    print(r)
    plt.show()

cand0 = [0.01,
np.array([[-0.55813952, -0.65813952], [-0.178139526, -0.65813952], [0.18186047400000002, -0.65813952], [0.49986048, -0.65813952], [0.61986048, -0.33813952], [0.31986048, -0.33813952], [-0.000139526368, -0.33813952], [-0.36013952, -0.33813952], [-0.7001395199999999, -0.33813952], [0.74986048, -0.0021395264], [0.44986048, -0.0021395264], [0.129860474, -0.0021395264], [-0.23013952000000001, -0.0021395264], [-0.5701395199999999, -0.0021395264]])]
cand0[1] = np.concatenate([cand0[1], cand0[1][:9] * [1, -1]]) * 1.1
print(cand0[1].tolist())
cand0 = [0.3019812207950221,
np.array([[-0.584, -0.704], [-0.156, -0.744], [0.16, -0.744], [0.5998, -0.694], [0.7518, -0.312], [0.3518, -0.332], [0.1098, -0.382], [-0.3662, -0.222], [-0.8302, -0.242], [0.7948, -0.0524], [0.4548, 0.0176], [0.1928, -0.1224], [-0.1832, 0.0976], [-0.6572, 0.0176], [-0.554, 0.714], [-0.066, 0.594], [-0.06, 0.854], [0.4198, 0.764], [0.8118, 0.422], [0.4718, 0.372], [0.0198, 0.422], [-0.3462, 0.292], [-0.8802, 0.282]])]
drawpoints(cand0)
