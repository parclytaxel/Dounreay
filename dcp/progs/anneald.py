#!/usr/bin/env python3.6
import numpy as np
from scipy.spatial import Delaunay, ConvexHull, cKDTree
from copy import deepcopy
N = 25
deltas = np.array([[0, 1], [1, 0], [0, -1], [-1, 0], [0, 2], [2, 0], [0, -2], [-2, 0], [1, 1], [-1, 1], [1, -1], [-1, -1]])

def ssq(x): return sum(x * x) # x.x, sum of squares or squared norm
def circumradius(verts): # Circumradius of given points
    l = [ssq(verts[i - 1] - verts[i - 2]) for i in (0, 1, 2)]
    l = [l[i] * (l[i - 2] + l[i - 1] - l[i]) for i in (0, 1, 2)]
    ccentre = np.dot(l, verts) / sum(l) # Barycentric formulation of the circumcentre
    if ssq(ccentre) > 1: return 0 # Triangle is close to the edge, will be dealt with by minedgeradius
    cradius = np.sqrt(ssq(verts[0] - ccentre))
    return cradius
def minedgeradius(verts):
    # For an edge close to the unit circle boundary and an isoceles triangle built on it,
    # computes the minimal leg length required
    m, p = (verts[0] + verts[1]) / 2, (verts[1] - verts[0]) @ np.array([[0, 1], [-1, 0]])
    # Equation of the line is m + pt, t in reals; intersect this with unit circle
    t, u = np.roots([ssq(p), sum(2 * p * m), ssq(m) - 1])
    d1, d2 = ssq(verts[0] - m + p * t), ssq(verts[1] - m + p * u)
    return np.sqrt(min(d1, d2))
def mcr(points):
    # Given the point set, returns the minimal covering radius (MCR).
    triangles = Delaunay(points).simplices
    hullring = ConvexHull(points).vertices
    hulledges = [[hullring[i], hullring[i - 1]] for i in range(len(hullring))]
    return max([circumradius(points[tri]) for tri in triangles] + [minedgeradius(points[edge]) for edge in hulledges])

def neighbour(points, F):
    # Given the points and fineness, generates a neighbouring point set and its MCR.
    # Fineness is the negative of log-temperature; it goes from 2 to 4.
    out = points.copy()
    choice = np.random.randint(len(points))
    delta = [3, 0]
    while ssq(out[choice] + delta) > 1: delta = deltas[np.random.randint(12)] / 10 ** F
    out[choice] += delta
    return out, mcr(out)
def accept(r_old, r_new, F): return np.random.rand() <= np.exp(10 ** F * min(r_old - r_new, 0))
def annealingrun(cand, F):
    # Anneals one candidate - a 3-tuple (points, features, radius).
    current, best = cand, cand
    accepts = 0
    while accepts < 100:
        new = neighbour(current[0], F)
        if accept(current[1], new[1], F):
            accepts += 1
            current = new
            if new[1] <= best[1]: best = new
    return best

def riuc(n = 1):
    # Random point(s) in unit circle with all points quantised to 0.01.
    theta = 2 * np.pi * np.random.rand(n)
    r = np.sqrt(0.85 * np.random.rand(n))
    return np.column_stack([r * np.cos(theta), r * np.sin(theta)]).round(2)
def initial():
    out = [riuc()[0]]
    while len(out) < N: # Mitchell's best-candidate used here
        cands = riuc(10)
        tree = cKDTree(out)
        out.append(cands[np.argmax(tree.query(cands)[0])])
    return cand_from_list(out)
def initial2():
    theta = 2 * np.pi * np.cumsum(np.random.dirichlet([8] * 17)) + np.random.rand()
    outerpoints = np.column_stack([0.9 * np.cos(theta), 0.9 * np.sin(theta)])
    theta = 2 * np.pi * np.cumsum(np.random.dirichlet([8] * 7))
    innerpoints = np.column_stack([0.4 * np.cos(theta), 0.4 * np.sin(theta)])
    return cand_from_list(np.concatenate([outerpoints, innerpoints, [[0, 0]]]))
def cand_from_list(l):
    # Generates an independent candidate from a nested list.
    nl = np.array(l)
    return nl, mcr(nl)
def generatecand():
    cand = initial2()
    for q in range(5): cand = annealingrun(cand, 2)
    p, r = cand
    triangles = Delaunay(p).simplices.tolist()
    hullring = ConvexHull(p).vertices
    boundaries = [[hullring[i - 1], hullring[i]] for i in range(len(hullring))]
    x0 = list(p.flatten()) + [r]
    return (x0, triangles, boundaries)
