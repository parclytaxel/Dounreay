#!/usr/bin/env python3.6
import numpy as np
from scipy.spatial import Delaunay, ConvexHull, cKDTree
from copy import deepcopy
N = 23
deltas = np.array([[0, 1], [1, 0], [0, -1], [-1, 0], [0, 2], [2, 0], [0, -2], [-2, 0], [1, 1], [-1, 1], [1, -1], [-1, -1]])

def ssq(x): return sum(x * x) # x.x, sum of squares or squared norm
def circumradius(verts): # Circumradius of given points
    l = [ssq(verts[i - 1] - verts[i - 2]) for i in (0, 1, 2)]
    l = [l[i] * (l[i - 2] + l[i - 1] - l[i]) for i in (0, 1, 2)]
    ccentre = np.dot(l, verts) / sum(l) # Barycentric formulation of the circumcentre
    if ssq(ccentre) > 1: return 0 # Triangle is close to the edge, will be dealt with by minedgeradius
    cradius = np.sqrt(ssq(verts[0] - ccentre))
    return cradius
def minedgeradius(verts):
    # For an edge close to the unit circle boundary and an isoceles triangle built on it,
    # computes the minimal leg length required
    m, p = (verts[0] + verts[1]) / 2, (verts[1] - verts[0]) @ np.array([[0, 1], [-1, 0]])
    # Equation of the line is m + pt, t in reals; intersect this with unit circle
    t, u = np.roots([ssq(p), sum(2 * p * m), ssq(m) - 1])
    d1, d2 = ssq(verts[0] - m + p * t), ssq(verts[1] - m + p * u)
    return np.sqrt(min(d1, d2))
def mcr(points):
    # Given the point set, returns the minimal covering radius (MCR).
    triangles = Delaunay(points).simplices
    hullring = ConvexHull(points).vertices
    hulledges = [[hullring[i], hullring[i - 1]] for i in range(len(hullring))]
    return max([circumradius(points[tri]) for tri in triangles] + [minedgeradius(points[edge]) for edge in hulledges])

def neighbour(points, F):
    # Given the points and fineness, generates a neighbouring point set and its MCR.
    # Fineness is the negative of log-temperature; it goes from 2 to 4.
    out = points.copy()
    choice = np.random.randint(len(points))
    delta = [3, 0]
    while ssq(out[choice] + delta) > 1: delta = deltas[np.random.randint(12)] / 10 ** F
    out[choice] += delta
    return out, mcr(out)
def accept(r_old, r_new, F): return np.random.rand() <= np.exp(10 ** F * min(r_old - r_new, 0))
def annealingrun(cand, F):
    # Anneals one candidate - a 3-tuple (points, features, radius).
    current, best = cand, cand
    accepts = 0
    while accepts < 100:
        new = neighbour(current[0], F)
        if accept(current[1], new[1], F):
            accepts += 1
            current = new
            if new[1] <= best[1]: best = new
    return best
def annealingrace(cands, F, niters, yoke=False):
    # Does len(cands) instances of annealingrun for niters iterations.
    # If yoke is True, sets all candidates to the best candidate after each iteration;
    # this is useful for squeezing out small improvements.
    for n in range(niters):
        cands = sorted([annealingrun(cand, F) for cand in cands], key=lambda x: x[1])
        if yoke:
            for i in range(1, len(cands)): cands[i] = deepcopy(cands[0])
            print(n, cands[0][1].item())
        else:
            print(n, [cand[1] for cand in cands])
    return cands

def riuc(n = 1):
    # Random point(s) in unit circle with all points quantised to 0.01.
    theta = 2 * np.pi * np.random.rand(n)
    r = np.sqrt(0.85 * np.random.rand(n))
    return np.column_stack([r * np.cos(theta), r * np.sin(theta)]).round(2)
def initial():
    out = [riuc()[0]]
    while len(out) < N: # Mitchell's best-candidate used here
        cands = riuc(10)
        tree = cKDTree(out)
        out.append(cands[np.argmax(tree.query(cands)[0])])
    return cand_from_list(out)
def initial2():
    # 12/6 distribution
    theta = 2 * np.pi * np.cumsum(np.random.dirichlet([5] * 12))
    outerpoints = np.column_stack([0.88 * np.cos(theta), 0.88 * np.sin(theta)])
    theta = 2 * np.pi * np.cumsum(np.random.dirichlet([5] * 6))
    innerpoints = np.column_stack([0.3 * np.cos(theta), 0.5 * np.sin(theta)])
    return cand_from_list(np.concatenate([outerpoints, innerpoints]))

def cand_from_list(l):
    # Generates an independent candidate from a nested list.
    nl = np.array(l)
    return nl, mcr(nl)

x0 = [[-0.6139534720000001, -0.723953472], [-0.1959534786, -0.723953472], [0.20004652140000004, -0.723953472], [0.549846528, -0.723953472], [0.6818465280000001, -0.37195347200000006], [0.35184652800000005, -0.37195347200000006], [-0.0001534790048, -0.37195347200000006], [-0.396153472, -0.37195347200000006], [-0.770153472, -0.37195347200000006], [0.8248465280000001, -0.00235347904], [0.49484652800000006, -0.00235347904], [0.1428465214, -0.00235347904], [-0.25315347200000005, -0.00235347904], [-0.6271534719999999, -0.00235347904], [-0.6139534720000001, 0.723953472], [-0.1959534786, 0.723953472], [0.20004652140000004, 0.723953472], [0.549846528, 0.723953472], [0.6818465280000001, 0.37195347200000006], [0.35184652800000005, 0.37195347200000006], [-0.0001534790048, 0.37195347200000006], [-0.396153472, 0.37195347200000006], [-0.770153472, 0.37195347200000006]]
cands = [cand_from_list(x0) for q in range(5)]
#cands = sorted([initial() for q in range(1000)], key=lambda x: x[1])[:5]
for q in range(100):
    cands = annealingrace(cands, 2, 50)
    with open(f"bestcoverings", 'w') as f:
        for i in range(len(cands)):
            f.write(f"cand{i} = [{cands[i][1]},\nnp.array({cands[i][0].round(4).tolist()})]\n")
