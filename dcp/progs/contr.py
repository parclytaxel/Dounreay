#!/usr/bin/env python3.6
from refine import refine, depict
from anneald import generatecand

best_r, best_z, i = 1, None, 0

try:
    for q in range(1):
        x0, triangles, boundaries = generatecand()
        try: z = refine(x0, triangles, boundaries)
        except RuntimeWarning: continue
        mcr = z.x[-1]
        if z.status == 0: print(f"({i}, {mcr})")
        if mcr < best_r:
            print("mcr =", mcr)
            best_r = mcr
            best_z = z
            depict(best_z.x, "25.svg")
        i += 1
except KeyboardInterrupt:
    print(best_z.x.tolist())
