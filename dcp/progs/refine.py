#!/usr/bin/env python3.6
import numpy as np
from scipy.optimize import minimize

def ssq(x): return sum(x * x)
# x0 y0
# x1 y1
# x2 y2
def circumradius(points, r):
    edges = np.array([ssq(points[i] - points[i - 1]) for i in range(3)])
    return r - np.sqrt(np.prod(edges) / (np.sum(edges) ** 2 - 2 * np.sum(edges ** 2)))
# x0 y0
# x1 y1
def boundarydist(points, r):
    diff = points[1] - points[0]
    k = np.sqrt(r ** 2 / ssq(diff) - 0.25)
    zx, zy = np.mean(points, 0)
    return np.hypot(zx + diff[1] * k, zy - diff[0] * k) - 1

def constrs(x, triangles, boundaries):
    points, r = x[:-1].reshape((len(x) // 2, 2)), x[-1]
    return [circumradius(points[triad], r) for triad in triangles] + [boundarydist(points[duad], r) for duad in boundaries]

def depict(x, fn="out.svg"):
    points, r = x[:-1].reshape((len(x) // 2, 2)), x[-1]
    out = '''<svg xmlns="http://www.w3.org/2000/svg" width="1020" height="1020" viewBox="-510 -510 1020 1020">
  <circle r="500" fill="none" stroke="#c0c0c0" stroke-width="4"/>\n'''
    r_ = r * 500
    for c in points:
        cx, cy = c[0] * 500, -c[1] * 500
        out += f'  <circle cx="{cx}" cy="{cy}" r="{r_}" fill="none" stroke="#000" stroke-width="4"/>\n'
    out += "</svg>"
    with open(fn, 'w') as f: f.write(out)

def refine(x0, triangles, boundaries):
    return minimize(lambda x: x[-1], x0, constraints=({"type": "ineq", "fun": constrs, "args": (triangles, boundaries)},), options={'ftol': 1e-12})
