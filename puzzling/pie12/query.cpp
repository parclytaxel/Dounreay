#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include "comb.h"
#define MAX_QTM 24
#define MAX_FTM 14

// To compile:
// g++ -Wall -O4 -c comb.cpp
// g++ -Wall -O4 comb.o query.cpp -o query
// Note that the two pie12 programs must have been run first
// Set MAX_QTM and MAX_FTM to 23 and 13 respectively for the 2-solution compilation

using namespace std;

const uint32_t ORDER = 479001600;
static uint8_t solqtm[ORDER / 4];
static uint8_t solftm[ORDER / 4];
static uint32_t table[MAX_QTM + 1][MAX_FTM + 1];
static uint8_t work[2][12];

uint8_t query_qtm(uint32_t N) {
    uint32_t sols_i = N >> 2;
    uint32_t sols_d = (N & 3) << 1;
    return solqtm[sols_i] >> sols_d & 3;
}

uint8_t query_ftm(uint32_t N) {
    uint32_t sols_i = N >> 2;
    uint32_t sols_d = (N & 3) << 1;
    return solftm[sols_i] >> sols_d & 3;
}

uint8_t min_moves_qtm(uint32_t N) {
    uint8_t res = 0, s = 0, val = query_qtm(N);
    decode(N, work[s]);
    while (val) {
        for (int t = 0; t < 4; ++t) {
            apply(work[s], t, work[!s]);
            uint8_t nval = query_qtm(encode(work[!s]));
            if (nval == 0 || (val == 3 && nval == 2)
                          || (val == 2 && nval == 1)
                          || (val == 1 && nval == 3)) {
                val = nval;
                s = !s;
                ++res;
                break;
            }
        }
    }
    return res;
}

uint8_t min_moves_ftm(uint32_t N) {
    uint8_t res = 0, s = 0, val = query_ftm(N);
    decode(N, work[s]);
    while (val) {
        for (int t = 0; t < 14; ++t) {
            apply_ftm(work[s], t, work[!s]);
            uint8_t nval = query_ftm(encode(work[!s]));
            if (nval == 0 || (val == 3 && nval == 2)
                          || (val == 2 && nval == 1)
                          || (val == 1 && nval == 3)) {
                val = nval;
                s = !s;
                ++res;
                break;
            }
        }
    }
    return res;
}

int main() {
    FILE* optqtm;
    if ((optqtm = fopen("optqtm", "rb")) == NULL) {
        printf("file \"optqtm\" not found\n");
        exit(1);
    }
    int rem = ORDER / 4;
    while ((rem -= fread(solqtm, 1, rem, optqtm)) > 0) {
        printf("%d bytes to read\n", rem);
    }
    fclose(optqtm);

    FILE* optftm;
    if ((optftm = fopen("optftm", "rb")) == NULL) {
        printf("file \"optftm\" not found\n");
        exit(1);
    }
    rem = ORDER / 4;
    while ((rem -= fread(solftm, 1, rem, optftm)) > 0) {
        printf("%d bytes to read\n", rem);
    }
    fclose(optftm);

    for (uint32_t N = 0; N < ORDER; ++N) {
        if (N % 1000000 == 0) {
            printf("%dM:\n", N / 1000000);
            for (int i = 0; i < MAX_QTM + 1; ++i) {
                for (int j = 0; j < MAX_FTM + 1; ++j) {
                    printf("%9d", table[i][j]);
                }
                printf("\n");
            }
        }
        ++table[min_moves_qtm(N)][min_moves_ftm(N)];
    }
    printf("Final:\n");
    for (int i = 0; i < MAX_QTM + 1; ++i) {
        for (int j = 0; j < MAX_FTM + 1; ++j) {
            printf("%9d", table[i][j]);
        }
        printf("\n");
    }
}
