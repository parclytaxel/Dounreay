#include <queue>
#include <cstdint>
#include <cstdio>
#include "comb.h"

// To compile:
// g++ -Wall -O4 -c comb.cpp
// g++ -Wall -O4 comb.o pie12-qtm.cpp -o pie12

using namespace std;

const uint32_t ORDER = 479001600;
static queue<uint32_t> q; // queue of states
static uint8_t sols[ORDER / 4] = {0}; // solution tree, using mod-3 trick
static uint8_t curr[12], neigh[12]; // current, neighbour positions
static uint8_t depth = 0; // depth of states currently in queue

// Use the upper two bits to store the inverse of last move made:
// 00, 01 = forward, backward rotation of first 8 elements
// 10, 11 = forward, backward rotation of last 8 elements
// The third-most significant bit is 0 except for the initial position(s),
// serving as a hint to the expansion function below.

// 01, 10, 11 in the mod-3 table sols represent a depth mod 3 of 0, 1, 2 respectively;
// 00 represents unseen states and the only two-bit field left as 00 at the end
// of the program corresponds to the solved state(s).
void expand() {
    uint32_t val = q.front();
    q.pop();
    uint8_t delta_lb = 1 - (val >> 29 & 1);
    decode(val & 0x1fffffff, curr);
    uint8_t inv_lastmove = val >> 30; // inverse of move taken to get here
    for (uint8_t delta = delta_lb; delta <= 3; ++delta) {
        uint8_t t = inv_lastmove ^ delta;
        apply(curr, t, neigh);
        uint32_t npos = encode(neigh);
        uint32_t sols_i = npos >> 2;
        uint32_t sols_d = (npos & 3) << 1;
        if (!(sols[sols_i] & 3 << sols_d)) {
            sols[sols_i] |= ((depth + 1) % 3 + 1) << sols_d;
            q.push((t ^ 1) << 30 | npos);
        }
    }
}

int main() {
    q.push(0x20000000);
    // q.push(0x3c8cfbff); // reversed position
    uint32_t depth_size = q.size(), total = 0;
    while (depth_size) {
        printf("%d %d\n", depth, depth_size);
        total += depth_size;
        for (uint32_t i = 0; i < depth_size; ++i) {
            // Uncomment below code to print antipodes;
            // put depth == 23 if reversed position is considered solved
            /*if (depth == 24) {
                decode(q.front() & 0x1fffffff, curr);
                for (int j = 0; j < 11; j++) {
                    printf("%u ", curr[j]);
                }
                printf("%u\n", curr[11]);
            }*/
            expand();
        }
        ++depth;
        depth_size = q.size();
    }
    printf("total %d\n", total);
    FILE* out = fopen("optqtm", "wb");
    int rem = ORDER / 4;
    while ((rem -= fwrite(sols, 1, rem, out)) > 0) {
        printf("%d bytes to write\n", rem);
    }
    fclose(out);
}
