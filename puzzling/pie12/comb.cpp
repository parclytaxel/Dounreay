#include <algorithm>
#include <cstdint>
#include <numeric>

using namespace std;

// Encode a permutation on 12 elements into a factoradic number,
// which requires 29 bits.
uint32_t encode(uint8_t state[12]) {
    uint32_t res = 0;
    for (int k = 0; k < 11; ++k) {
        uint8_t invs = count_if(state + k + 1, state + 12, [=] (int i) {
            return state[k] > i;
        });
        res = res * (12 - k) + invs;
    }
    return res;
}

const uint32_t FACS[11] = {39916800, 3628800, 362880, 40320, 5040, 720, 120, 24, 6, 2, 1};
// Decode a factoradic number into a provided state array.
void decode(uint32_t N, uint8_t state[12]) {
    iota(state, state + 12, 0);
    for (int k = 0; k < 11; ++k) {
        uint8_t i = N / FACS[k];
        rotate(state + k, state + k + i, state + k + i + 1);
        N %= FACS[k];
    }
}

const uint8_t perm[4][12] =
{{1,2,3,4,5,6,7,0,8,9,10,11},  // rotate first 8 right
 {7,0,1,2,3,4,5,6,8,9,10,11},  // rotate first 8 left
 {0,1,2,3,5,6,7,8,9,10,11,4},  // rotate last 8 right
 {0,1,2,3,11,4,5,6,7,8,9,10}}; // rotate last 8 left

// Apply the permutation indexed by t on start, storing the result in end.
void apply(uint8_t start[12], uint8_t t, uint8_t end[12]) {
    for (int i = 0; i < 12; i++) {
        end[perm[t][i]] = start[i];
    }
}

const uint8_t perm_ftm[14][12] =
{{1,2,3,4,5,6,7,0,8,9,10,11},
 {2,3,4,5,6,7,0,1,8,9,10,11},
 {3,4,5,6,7,0,1,2,8,9,10,11},
 {4,5,6,7,0,1,2,3,8,9,10,11},
 {5,6,7,0,1,2,3,4,8,9,10,11},
 {6,7,0,1,2,3,4,5,8,9,10,11},
 {7,0,1,2,3,4,5,6,8,9,10,11},
 {0,1,2,3,5,6,7,8,9,10,11,4},
 {0,1,2,3,6,7,8,9,10,11,4,5},
 {0,1,2,3,7,8,9,10,11,4,5,6},
 {0,1,2,3,8,9,10,11,4,5,6,7},
 {0,1,2,3,9,10,11,4,5,6,7,8},
 {0,1,2,3,10,11,4,5,6,7,8,9},
 {0,1,2,3,11,4,5,6,7,8,9,10}};

void apply_ftm(uint8_t start[12], uint8_t t, uint8_t end[12]) {
    for (int i = 0; i < 12; i++) {
        end[perm_ftm[t][i]] = start[i];
    }
}
