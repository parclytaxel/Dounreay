#include <cstdint>

uint32_t encode(uint8_t state[12]);
void decode(uint32_t N, uint8_t state[12]);
void apply(uint8_t start[12], uint8_t t, uint8_t end[12]);
void apply_ftm(uint8_t start[12], uint8_t t, uint8_t end[12]);
