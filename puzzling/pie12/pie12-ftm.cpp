#include <queue>
#include <cstdint>
#include <cstdio>
#include "comb.h"

// To compile:
// g++ -Wall -O4 -c comb.cpp
// g++ -Wall -O4 comb.o pie12-ftm.cpp -o pie12

using namespace std;

const uint32_t ORDER = 479001600;
static queue<uint32_t> q; // queue of states
static uint8_t sols[ORDER / 4] = {0}; // solution tree, using mod-3 trick
static uint8_t curr[12], neigh[12]; // current, neighbour positions
static uint8_t depth = 0; // depth of states currently in queue

// Define an L/R-position as a state in the search tree ending in L/R
// and nL/R as the set of all L/R-positions at depth n.
// Face turns must necessarily alternate, so elements get pushed in as follows:
// 0R, 0L, 1L, 1R, 2R, 2L, 3L, 3R, 4R, 4L...
// As such there is no need to keep track of the last used move within the queue.

// 01, 10, 11 in the mod-3 table sols represent a depth mod 3 of 0, 1, 2 respectively;
// 00 represents unseen states and the only two-bit field left as 00 at the end
// of the program corresponds to the solved state(s).

// If side = 0, expands on the left disc and only operates on R-positions.
// If side = 1, expands on the right disc and only operates on L-positions.
void expand(uint8_t side) {
    uint32_t val = q.front();
    q.pop();
    decode(val, curr);
    for (uint8_t t = 7 * side; t < 7 * (1 + side); ++t) {
        apply_ftm(curr, t, neigh);
        uint32_t npos = encode(neigh);
        uint32_t sols_i = npos >> 2;
        uint32_t sols_d = (npos & 3) << 1;
        if (!(sols[sols_i] & 3 << sols_d)) {
            sols[sols_i] |= ((depth + 1) % 3 + 1) << sols_d;
            q.push(npos);
        }
    }
}

int main() {
    q.push(0);
    // q.push(ORDER - 1);
    q.push(0);
    // q.push(ORDER - 1);
    uint32_t depth_size_first = q.size() / 2, depth_size_second = q.size() / 2, total = 0;
    while (depth_size_first + depth_size_second) {
        if (depth) {
            printf("%d %d\n", depth, depth_size_first + depth_size_second);
            total += depth_size_first + depth_size_second;
        } else {
            printf("0 %d\n", q.size() / 2);
            total += q.size() / 2;
        }
        uint8_t dparity = depth & 1;
        for (uint32_t i = 0; i < depth_size_first; ++i) {
            expand(dparity);
        }
        depth_size_first = q.size() - depth_size_second;
        for (uint32_t i = 0; i < depth_size_second; ++i) {
            expand(!dparity);
        }
        depth_size_second = q.size() - depth_size_first;
        ++depth;
    }
    printf("total %d\n", total);
    FILE* out = fopen("optftm", "wb");
    int rem = ORDER / 4;
    while ((rem -= fwrite(sols, 1, rem, out)) > 0) {
        printf("%d bytes to write\n", rem);
    }
    fclose(out);
}
