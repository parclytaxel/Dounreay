These commands recreate the image _The Tapestry of Cohan_ on Linux distributions:

Compile source files. The last one is not necessary for the picture, but allows you to actually solve any position optimally:
```
gcc -Wall -O4 -c comb.c
gcc -Wall -O4 comb.o cohan.c -o cohan
gcc -Wall -O4 comb.o query.c -o query
```

Generate the optimal table. This is a good workout for your computer, and should take around five minutes (or less):
```
time ./cohan
```

Append a Netpbm header to this 81-megabyte binary file, so that it is interpreted as an image:
```
cat <(echo "P6 2261 12012 255") opt > opt.ppm
```

Use ImageMagick to convert to PNG and transpose so that it fits a laptop screen better:
```
convert-im6.q16 opt.ppm -transpose opt.png
```
