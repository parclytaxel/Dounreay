#include <stdint.h>

uint32_t encode(uint8_t state[13]);

void decode(uint32_t N, uint8_t state[13]);

void apply_perm(uint8_t start[13], uint8_t t, uint8_t end[13]);

void state2alph(uint8_t state[13], char res[14]);

void alph2state(char res[14], uint8_t state[13]);
