#include <stdint.h>
#include <stdlib.h>
#define CMPX(x, y) d = a[x] > a[y] ? a[x] ^ a[y] : 0; a[x] ^= d; a[y] ^= d
// Sort six elements starting from the given pointer using a sorting network.
// Twelve comparisons are needed, and this is optimal.
void sort6(uint8_t* a) {
    uint8_t d;
    CMPX(0,5); CMPX(1,3); CMPX(2,4); CMPX(1,2); CMPX(3,4); CMPX(0,3);
    CMPX(2,5); CMPX(0,1); CMPX(2,3); CMPX(4,5); CMPX(1,2); CMPX(3,4);
}

/* Encoding and decoding Cohan Circle states */

// A state is an array of 13 distinct numbers from 0 to 18.
// The first six are the locations of the left circle pieces (deep purple),
// the next six of the right circle (cobalt blue), the last the centre (yellow).
//      .    0    .    1    .
//    2    3    4    5    6    7
// .    8    .    9    .   10    .
//   11   12   13   14   15   16
//      .   17    .   18    .
// The numbers within each block are in INCREASING order. This gives
// binom(19,6) * binom(13,6) * 7 = 325909584 possible arrays, which is also
// the number of positions of the Cohan Circle.

// A state is encoded into an integer as (1716*27132)C + (27132)B + A, where
// A is the combinadic code of the array's first six entries (0 to 27131);
// B is the combinadic code of the next six entries (0 to 1715) if indexing
// skips over A's entries;
// C is the index of the last entry (0 to 6) if indexing skips over A's and B's entries.
// See https://en.wikipedia.org/wiki/Combinatorial_number_system
// for an explanation of the combinadic code.

// e.g. {0, 4, 6, 9, 11, 13, 2, 7, 8, 15, 16, 17, 3} encodes to 69812966:
// 0  =  =  =  4  =  6  =  =  9  = 11  = 13  =  =  =  =  = <- A
//    =  2  =     =     7  8     =     =     = 15 16 17  = <- B
//    =     3     =              =     =     =           = <- C
// A = binom(0,1) + binom(4,2) + binom(6,3) + binom(9,4) + binom(11,5) + binom(13,6) = 2330
// B = binom(1,1) + binom(4,2) + binom(5,3) + binom(9,4) + binom(10,5) + binom(11,6) = 857
// C = 1, so the number is (1716*27132)*3 + (27132)*857 + 2330 = 69812966.

// binom[n][k-1] for 0 <= n < 19, 1 <= k <= 6
const uint16_t binom[19][6] = {{0,0,0,0,0,0}, {1,0,0,0,0,0}, {2,1,0,0,0,0}, {3,3,1,0,0,0},
{4,6,4,1,0,0}, {5,10,10,5,1,0}, {6,15,20,15,6,1}, {7,21,35,35,21,7}, {8,28,56,70,56,28},
{9,36,84,126,126,84}, {10,45,120,210,252,210}, {11,55,165,330,462,462}, {12,66,220,495,792,924},
{13,78,286,715,1287,1716}, {14,91,364,1001,2002,3003}, {15,105,455,1365,3003,5005},
{16,120,560,1820,4368,8008}, {17,136,680,2380,6188,12376}, {18,153,816,3060,8568,18564}};
const uint16_t LEFT = 27132;
const uint16_t RIGHT = 1716;

// Encode a state into a number.
uint32_t encode(uint8_t state[13]) {
    uint16_t A = 0, B = 0, C = 0;
    // Merge-sort the two blocks of six; in doing so calculate A, B, C.
    // i indexes into the first block, j into the second.
    int i = 0, j = 0;
    for (int n = 0; n < 19; n++) {
        if (n == state[i] && i < 6) {
            A += binom[n][i++];
        } else if (n == state[6+j] && j < 6) {
            B += binom[n-i][j++];
        } else if (n < state[12]) {
            C++;
        }
    }
    // printf("A = %d, B = %d, C = %d\n", A, B, C);
    return (C * RIGHT + B) * LEFT + A;
}

// Decode a number into a state, where the output array is provided
// as part of the arguments.
void decode(uint32_t N, uint8_t state[13]) {
    uint16_t A = N % LEFT, B = N / LEFT % RIGHT, C = N / (LEFT * RIGHT);
    // Essentially the reverse of the encoding procedure.
    int i = 5, j = 5;
    for (int n = 18; n >= 0; n--) {
        if (A >= binom[n][i] && i >= 0) {
            A -= binom[n][i];
            state[i--] = n;
        } else if (B >= binom[n-i-1][j] && j >= 0) {
            B -= binom[n-i-1][j];
            state[6+j--] = n;
        } else if (C == n-i-j-2) {
            state[12] = n;
        }
    }
}

// Convert a state into the human-friendly format used on my
// Cohan Circle/Arusloky simulator on Observable
// (https://observablehq.com/@parcly-taxel/cohan-circle).
// 0 = a, 1 = b, 2 = c, ..., 18 = s.
void state2alph(uint8_t state[13], char res[14]) {
    for (int i = 0; i < 13; i++) {
        res[i] = state[i] + 0x61;
    }
    res[13] = 0;
}

// Convert from the human-friendly format back into a state.
// Out-of-order letters in the two blocks of six will be kindly sorted.
void alph2state(char res[14], uint8_t state[13]) {
    for (int i = 0; i < 13; i++) {
        state[i] = res[i] - 0x61;
    }
    sort6(state);
    sort6(state + 6);
}

// Permutations representing sixth turns; L, L', R, R' in order.
const uint8_t perm[4][19] =
{{5,1,0,4,9,14,6,7,3,13,10,2,8,12,17,15,16,11,18},
 {2,1,11,8,3,0,6,7,12,4,10,17,13,9,5,15,16,14,18},
 {0,7,2,3,1,6,10,16,8,5,15,11,12,4,9,14,18,17,13},
 {0,4,2,3,13,9,5,1,8,14,6,11,12,18,15,10,7,17,16}};

// Apply the permutation indexed by t on start, storing the result in end.
// The two six-element block sorts needed to recover canonical representation
// are implemented by sort6 (see above).
void apply_perm(uint8_t start[13], uint8_t t, uint8_t end[13]) {
    for (int i = 0; i < 13; i++) {
        end[i] = perm[t][start[i]];
    }
    sort6(end);
    sort6(end + 6);
}
