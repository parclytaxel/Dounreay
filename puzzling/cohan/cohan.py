#!/usr/bin/env python3.7
from collections import deque
import heapq

# Phase 1 - <L,R>
# Each 7-set represents the positions of the deep purple pieces
#      .    0    .    1    .
#    2    3    4    5    6    7
# .    8    .    9    .   10    .
#   11   12   13   14   15   16
#      .   17    .   18    .

perm = {0: [5, 1, 0, 4, 9, 14, 6, 7, 3, 13, 10, 2, 8, 12, 17, 15, 16, 11, 18], # L
        1: [2, 1, 11, 8, 3, 0, 6, 7, 12, 4, 10, 17, 13, 9, 5, 15, 16, 14, 18], # L'
        2: [0, 4, 2, 3, 13, 9, 5, 1, 8, 14, 6, 11, 12, 18, 15, 10, 7, 17, 16], # R'
        3: [0, 7, 2, 3, 1, 6, 10, 16, 8, 5, 15, 11, 12, 4, 9, 14, 18, 17, 13], # R
        4: [0, 13, 2, 3, 18, 14, 9, 4, 8, 15, 5, 11, 12, 16, 10, 6, 1, 17, 7], # R'R'
        5: [0, 16, 2, 3, 7, 10, 15, 18, 8, 6, 14, 11, 12, 1, 5, 9, 13, 17, 4], # RR
        6: [14, 1, 5, 9, 13, 17, 6, 7, 4, 12, 10, 0, 3, 8, 11, 15, 16, 2, 18], # LL
        7: [11, 1, 17, 12, 8, 2, 6, 7, 13, 3, 10, 14, 9, 4, 0, 15, 16, 5, 18]} # L'L'
inverse = {0: 1, 1: 0, 2: 3, 3: 2, 4: 5, 5: 4, 6: 7, 7: 6}

def turn(curr, direc):
    return frozenset(perm[direc][pos] for pos in curr)

"""
# {"1" piece locations: (distance from goal, move needed to bring it closer)}
seen = {frozenset({0, 2, 5, 10, 11, 14, 17}): (0, ".")}
queue = deque([(frozenset({0, 2, 5, 10, 11, 14, 17}), 0, ".")])
while queue:
    curr, dist, pred = queue.popleft()
    for direc in range(4):
        neighbour = turn(curr, direc)
        if neighbour not in seen:
            seen[neighbour] = (dist + 1, inverse[direc])
            queue.append((neighbour, dist + 1, inverse[direc]))

for (k, v) in seen.items():
    print(",".join(str(n) for n in sorted(k)), v[1])
"""

# Phase 2 - <L,R2>
# Each 7-set represents the positions of the cobalt blue pieces
#      .    -    .    1    .
#    /    3    4    \    6    7
# .    8    .    9    .    -    .
#    \   12   13    /   15   16
#      .    -    .   18    .

"""
seen = {frozenset({1, 4, 7, 8, 13, 16, 18}): (0, ".")}
pq = [(0, frozenset({1, 4, 7, 8, 13, 16, 18}))]
while pq:
    dist, curr = heapq.heappop(pq)
    if dist > seen[curr][0]: continue
    for direc in (0, 1, 4, 5):
        neighbour = turn(curr, direc)
        newdist = dist + (1 if direc in (0, 1) else 2)
        if neighbour not in seen or newdist < seen[neighbour][0]:
            seen[neighbour] = (newdist, inverse[direc])
            heapq.heappush(pq, (newdist, neighbour))

for (k, v) in seen.items():
    print(",".join(str(n) for n in sorted(k)), v[1])
"""

# Phase 3 - <L2,R2>
# Each 3-tuple represents (positions of light blue pieces in extended circles,
# *Cohan Circle's yellow piece*). The goal is (8, 9, 10).
# Note that each piece is stuck in its own orbit.
#      .    0    .    1    .
#    2   *3*   4    5   *6*   7
# .    8    .   *9*   .   10    .
#   11  *12*  13   14  *15*  16
#      .   17    .   18    .

def dedouble(n):
    return {4: [2, 2], 5: [3, 3], 6: [0, 0], 7: [1, 1]}[n]

seen = {frozenset({8, 9, 10}): []}
queue = deque([(frozenset({8, 9, 10}), [])])
while queue:
    curr, pred = queue.popleft()
    for direc in (4, 5, 6, 7):
        neighbour = turn(curr, direc)
        if neighbour not in seen:
            seen[neighbour] = dedouble(inverse[direc]) + pred
            queue.append((neighbour, seen[neighbour]))

for (k, v) in seen.items():
    print(",".join(str(n) for n in sorted(k)), " ".join(str(n) for n in v))
