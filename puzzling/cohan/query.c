#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comb.h"
const uint32_t FINAL_N = 180849499;
const char* TURNS[4] = {"L", "L'", "R", "R'"};

// Solve a Cohan Circle state optimally with a pointer into the table file opt.
// The solution itself is put in sol and the number of moves is returned.
uint8_t solve_cohan(uint8_t state[13], FILE* opt, uint8_t* sol) {
    uint32_t npos = encode(state);
    uint8_t curr[13];
    uint8_t N = 0;
    decode(npos, curr);
    while (npos != FINAL_N) {
        fseek(opt, npos / 4, SEEK_SET);
        uint8_t mod = npos % 4 * 2;
        uint8_t t = (fgetc(opt) & 3 << mod) >> mod;
        sol[N++] = t;
        apply_perm(curr, t, curr);
        npos = encode(curr);
    }
    return N;
}

// ./query sparkledfinch
// R R L' R' R' L R R L R' R' L R' L R' R' L' (length 17)
int main(int argc, char* argv[]) {
    if (argc != 2 || strlen(argv[1]) != 13) {
        printf("Usage: %s [position code, e.g. acgmorbklpqsd]\n", argv[0]);
        printf("Position code has 13 distinct lowercase letters a-s\n");
        exit(1);
    }
    uint8_t state[13];
    alph2state(argv[1], state);
    
    uint8_t used[19] = {0};
    for (int i = 0; i < 13; i++) {
        if (state[i] > 18 || used[state[i]]++) {
            printf("Invalid position code\n");
            exit(1);
        }
    }
    
    FILE* opt;
    if ((opt = fopen("opt", "rb")) == NULL) {
        printf("file \"opt\" not found\n");
        exit(1);
    }
    uint8_t sol[23];
    uint8_t moves = solve_cohan(state, opt, sol);
    for (int i = 0; i < moves; i++) {
        printf("%s ", TURNS[sol[i]]);
    }
    printf("(length %d)\n", moves);
    fclose(opt);
}
