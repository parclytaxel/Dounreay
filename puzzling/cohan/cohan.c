#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "comb.h"
// 180849499 = encode({0, 2, 5, 11, 14, 17, 1, 4, 7, 13, 16, 18, 9}),
// or the starting position of Cohan Circle
const uint32_t START_N = 180849499;
// binom(19,6) * binom(13,6) * 7 = 325909584, the number of positions
// (i.e. the order of the graph we are traversing)
const uint32_t ORDER = 325909584;

// Singly linked list node structure used for the BFS queue.
// The upper two bits of entry store the move performed on the position
// in the optimal solution (00 = L, 01 = L', 10 = R, 11 = R').
typedef struct node {
    uint32_t entry;
    struct node* next;
} node;

// (enqueue here) last <-- o <-- ... <-- o <-- first (dequeue here)
int main() {
    // Linked list nodes
    node* origin = malloc(sizeof(node));
    origin->entry = START_N;
    node* first = origin;
    node* last = origin;
    node* tmp;
    // Current and neighbour positions
    uint8_t curr[13], neigh[13];
    // Bitfield representing the optimal solution
    uint8_t* sols = calloc(ORDER / 4, 1);
    // sols[START_N / 4] = 0 << (START_N % 4 * 2);
    // Bitfield representing seen positions - the length is just right!
    uint8_t* seen = calloc(ORDER / 8, 1);
    seen[START_N / 8] = 1 << (START_N % 8);
    
    // Put in the four positions at one move from the origin
    for (uint8_t t = 0; t < 4; t++) {
        decode(first->entry, curr);
        apply_perm(curr, t, neigh);
        uint32_t npos = encode(neigh);
        sols[npos >> 2] |= (t ^ 1) << ((npos & 3) << 1);
        seen[npos >> 3] |= 1 << (npos & 7);
        node* enq = malloc(sizeof(node));
        enq->entry = (t ^ 1) << 30 | npos;
        last = last->next = enq;
    }
    first = first->next;
    free(origin); // It's Django Unchained
    
    uint32_t V = 1; // Number of entries popped out of the queue
    while (first != NULL) {
        // Look at the head of the queue, extract last move and position
        uint32_t val = first->entry;
        decode(val & 0x1fffffff, curr);
        uint8_t lm = val >> 30; // inverse of move taken to get here; don't do it
        for (uint8_t delta = 1; delta <= 3; delta++) {
            uint8_t t = lm ^ delta;
            apply_perm(curr, t, neigh);
            uint32_t npos = encode(neigh);
            if (!(seen[npos >> 3] & 1 << (npos & 7))) { // not seen?
                sols[npos >> 2] |= (t ^ 1) << ((npos & 3) << 1);
                seen[npos >> 3] |= 1 << (npos & 7);
                node* enq = malloc(sizeof(node));
                enq->entry = (t ^ 1) << 30 | npos;
                last = last->next = enq;
            }
        }
        tmp = first;
        first = first->next;
        free(tmp);
        if (++V % 1000000 == 0) {
            printf("V = %d\n", V);
        }
    }
    printf("search completed after processing %d positions\n", V);
    // Check whether all positions have been seen
    for (int i = 0; i < ORDER / 8; i++) {
        if (seen[i] != 255) {
            printf("seen byte at position %d not filled!\n", i);
            exit(1);
        }
    }
    free(seen);
    printf("all positions verified to be seen\n");
    // Write solutions to output
    FILE* out = fopen("opt", "wb");
    int rem = ORDER / 4;
    while ((rem -= fwrite(sols, 1, rem, out)) > 0) {
        printf("%d bytes to write\n", rem);
    }
    fclose(out);
    free(sols);
    printf("done\n");
    // Timings on a Lenovo U41:
    // real    5m33.092s
    // user    5m31.025s
    // sys     0m1.636s
}
