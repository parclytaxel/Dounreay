#!/usr/bin/env python3.7
from xml.dom import minidom
import xml.etree.ElementTree as ET
from mpmath import mp
mp.dps = 40
import poincaredisk as pdm
import pdmconst as pdc

class TilingGen:
    def __init__(self, p, q):
        if 2*(p+q) >= p*q:
            raise ValueError("parameters do not define a hyperbolic domain")
        self.p, self.q = max(p,q), min(p,q)
        if (self.p,self.q) == (7,3):
            self.edges, self.faces, self.vertices = pdm.make_general_graph(7,3)
        elif (self.p,self.q) == (5,4):
            self.edges, self.faces, self.vertices = pdm.make_pent_graph()
        elif (self.p,self.q) == (8,3):
            self.edges, self.faces, self.vertices = pdm.make_oct_graph()
        else:
            self.edges, self.faces, self.vertices = pdm.make_general_graph(self.p,self.q)
        self.s1 = pdc.side(self.q, self.p)
        self.c1 = mp.rect(pdc.circumradius(self.p, self.q), mp.pi/self.p)

    # The drawing methods use an instance variable self.work
    # that is the SVG tree of the tiling under construction.

    def patch_edge(self, e, locations, name, fill, width, curvedends=False):
        """Patch edge e at the given locations in the workspace."""
        gen = pdm.patch(e, locations)
        path_data = "\n".join(pdm.thick_segment(*t, width=width, curvedends=curvedends) for t in gen)
        self.work.append(pdm.draw_path(path_data, fill, name))
        self.work.extend(pdm.draw_clones(name, self.p))

    def patch_polygon(self, p, locations, name, fill):
        """Patch polygon p at the given locations in the workspace."""
        gen = pdm.patch(p, locations)
        path_data = "\n".join(pdm.render_polygon(t) for t in gen)
        self.work.append(pdm.draw_path(path_data, fill, name))
        self.work.extend(pdm.draw_clones(name, self.p))

    def draw_central_polygon(self, p, fill):
        """Draw the single polygon p onto the workspace with the given fill."""
        self.work.append(pdm.draw_path(pdm.render_polygon(p), fill))

    def write_work(self, filename):
        """Write this class's workspace to disk. The full filename
        prepends an order descriptor."""
        pdm.write_node(self.work, f"H2-{self.p}-{self.q}-{filename}.svg")

    def draw_primal(self):
        self.work = pdm.make_root_circle("#ff0")
        e = (0, self.s1)
        self.patch_edge(e, self.edges, "A", "blue", 0.012)
        self.write_work("primal")

    def draw_dual(self):
        self.work = pdm.make_root_circle("red")
        e = (self.c1, mp.conj(self.c1))
        self.patch_edge(e, self.edges, "A", "blue", 0.012)
        self.write_work("dual")

    def draw_trunc_dual(self):
        self.work = pdm.make_root_circle("red")
        e = (self.c1, mp.conj(self.c1))
        r0 = pdc.truncated_2p_radius(self.p, self.q)
        a0 = mp.rect(r0, mp.pi/(2*self.p))
        a = [pdm.rotatepi(a0, pdc.F(2*k,self.q), self.c1) for k in range(self.q)]
        self.patch_edge(e, self.edges, "A", "blue", 0.0075)
        self.patch_polygon(a, self.faces, "B", "#ff0")
        self.write_work("trunc-dual")

    def draw_rectified(self):
        self.work = pdm.make_root_circle("red")
        a0 = pdc.apothem(self.p, self.q)
        a = [pdm.rotatepi(a0, pdc.F(2*k,self.q), self.c1) for k in range(self.q)]
        self.patch_polygon(a, self.faces, "B", "#ff0")
        self.write_work("rectified")

    def draw_trunc_primal(self):
        self.work = pdm.make_root_circle("#ff0")
        R = pdc.truncated_q_radius(self.q, self.p)
        p0 = [R*z for z in mp.unitroots(self.p)]
        p1 = [pdm.translate(-v, self.s1) for v in p0]
        e = (0, self.s1)
        self.patch_edge(e, self.edges, "A", "blue", 0.0075)
        self.draw_central_polygon(p0, "red")
        self.patch_polygon(p1, self.vertices, "B", "red")
        self.write_work("trunc-primal")

    def draw_cantellated(self):
        self.work = pdm.make_root_circle("red")
        a0 = mp.rect(pdc.cantellated_p_radius(self.p, self.q), mp.pi/self.p)
        poly = [pdm.rotatepi(a0, pdc.F(2*k,self.q), self.c1) for k in range(self.q)]
        m = pdc.apothem(self.p, self.q)
        square = [pdm.rotatepi(a0, k/2, m) for k in range(4)]
        self.patch_polygon(square, self.edges, "A", "blue")
        self.patch_polygon(poly, self.faces, "B", "#ff0")
        self.write_work("cantellated")

    def draw_omnitruncated(self):
        self.work = pdm.make_root_circle("red")
        a0 = mp.rect(pdc.omnitruncated_p_radius(self.p, self.q), mp.pi/(2*self.p))
        poly = [pdm.rotatepi(a0, pdc.F(k,self.q), self.c1) for k in range(2*self.q)]
        m = pdc.apothem(self.p, self.q)
        square = [pdm.rotatepi(a0, k/2, m) for k in range(4)]
        self.patch_polygon(square, self.edges, "A", "blue")
        self.patch_polygon(poly, self.faces, "B", "#ff0")
        self.write_work("omnitruncated")

    def draw_snub(self):
        self.work = pdm.make_root_circle("cyan")
        Z = pdc.snub_p_vector(self.p, self.q)
        p0 = [Z*w for w in mp.unitroots(self.p)]
        p1 = [pdm.translate(-v, self.s1) for v in p0]
        p2 = [pdm.rotatepi(Z, pdc.F(2*k,self.q), self.c1) for k in range(self.q)]
        e = (Z, p1[0])
        self.patch_edge(e, self.edges, "A", "#000", 0.003, curvedends=True)
        self.draw_central_polygon(p0, "red")
        self.patch_polygon(p1, self.vertices, "B", "red")
        self.patch_polygon(p2, self.faces, "C", "#ff0")
        self.write_work("snub")

    def draw_kis_primal(self):
        self.work = pdm.make_root_circle("#c0c0ff")
        for k in range(self.q):
            b = pdm.rotatepi(0, pdc.F(2*k,self.q), self.c1)
            self.patch_edge((self.c1, b), self.faces, "A"+str(k), "#000", 0.0075)
        e = (0, self.s1)
        self.patch_edge(e, self.edges, "A", "#000", 0.0075)
        self.write_work("kis-primal")

    def draw_rhombic(self):
        self.work = pdm.make_root_circle("#ffff80")
        for k in range(self.q):
            b = pdm.rotatepi(0, pdc.F(2*k,self.q), self.c1)
            self.patch_edge((self.c1, b), self.faces, "A"+str(k), "#000", 0.0075)
        self.write_work("rhombic")

    def draw_kis_dual(self):
        self.work = pdm.make_root_circle("#fd8")
        for k in range(self.q):
            b = pdm.rotatepi(0, pdc.F(2*k,self.q), self.c1)
            self.patch_edge((self.c1, b), self.faces, "A"+str(k), "#000", 0.0075)
        e = (self.c1, mp.conj(self.c1))
        self.patch_edge(e, self.edges, "A", "#000", 0.0075)
        self.write_work("kis-dual")

    def draw_kisrhombille(self):
        self.work = pdm.make_root_circle("#2020ff")
        bolt = (0, self.s1, mp.conj(self.c1), self.c1)
        self.patch_polygon(bolt, self.edges, "A", "#fff")
        self.write_work("kisrhombille")

    def draw_deltoidal(self):
        self.work = pdm.make_root_circle("#c0ffc0")
        e = (self.c1, mp.conj(self.c1))
        s = (0, self.s1)
        self.patch_edge(s, self.edges, "A", "#000", 0.009)
        self.patch_edge(e, self.edges, "B", "#000", 0.007)
        self.write_work("deltoidal")

    def draw_floret(self):
        self.work = pdm.make_root_circle("#c0ffff")
        Z = pdc.snub_p_vector(self.p, self.q)
        th = mp.arg(Z) - mp.pi/self.p
        v1 = mp.rect(pdc.floret_constant(self.p, self.q), th)        
        v2 = pdm.rotatepi(v1, 1, pdc.apothem(self.p, self.q))
        v3 = mp.conj(self.c1)
        v4 = self.c1
        for (n, e) in enumerate(((0,v1), (v1,v3),
                (self.s1,v2), (v2,v4), (v1,v2))):
            self.patch_edge(e, self.edges, f"A{n}", "#000", 0.006)
        self.write_work("floret")

    def draw_all(self):
        self.draw_primal()
        self.draw_trunc_primal()
        self.draw_rectified()
        self.draw_trunc_dual()
        self.draw_dual()
        self.draw_cantellated()
        self.draw_omnitruncated()
        self.draw_snub()
        # duals to the above
        self.draw_kis_primal()
        self.draw_rhombic()
        self.draw_kis_dual()
        self.draw_deltoidal()
        self.draw_kisrhombille()
        self.draw_floret()
