#!/usr/bin/env python3.6
from mpmath import mp, sqrt, j, fraction, workdps
mp.dps = 40
scale = 1000
s3d2 = sqrt(3) / 2

def mt(frac): return ((0.5 - frac) * j - s3d2) / ((frac - 0.5) * j - s3d2)
def formpath(triple):
    dists = [abs(triple[j - 1] - triple[j]) for j in range(3)]
    rads = [d / sqrt(4 - d * d) * scale for d in dists]
    triple = [point * scale for point in triple]
    with workdps(15):
        x1 = f'<path d="M{triple[2].real} {triple[2].imag}A{rads[0]} {rads[0]} 0 0 1 {triple[0].real} {triple[0].imag}'
        x2 = f' {rads[1]} {rads[1]} 0 0 0 {triple[1].real} {triple[1].imag}'
        x3 = f' {rads[2]} {rads[2]} 0 0 0 {triple[2].real} {triple[2].imag}Z"/>'
    return x1 + x2 + x3

s, strlist = [[(1, 2), (2, 3), (1, 1)]], []
while s:
    fracs = s.pop()
    triple = [mt(fraction(*frac)) for frac in fracs]
    if abs(triple[0] - triple[2]) >= fraction(1, scale):
        strlist.append(formpath(triple))
        for q in range(2):
            fracs2 = [(fracs[i][0] + fracs[i + 1][0], fracs[i][1] + fracs[i + 1][1]) for i in range(len(fracs) - 1)]
            fracs2.append(None)
            fracs = [val for pair in zip(fracs, fracs2) for val in pair][:-1]
        s.extend([fracs[2 * i:2 * i + 3] for i in range(4)])
insert = "\n      ".join(strlist)
with open("in.svg", 'r') as f: raw = f.read()
out = raw.replace("<!---->", insert)
with open("out.svg", 'w') as f: f.write(out)