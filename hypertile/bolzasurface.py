#!/usr/bin/env python3.7
from xml.dom import minidom
import xml.etree.ElementTree as ET
from mpmath import mp
mp.dps = 40
F = mp.fraction
import poincaredisk as pdm
import pdmconst as pdc

SVG = "{http://www.w3.org/2000/svg}"
XLN = "{http://www.w3.org/1999/xlink}"
ET.register_namespace("", SVG[1:-1])
ET.register_namespace("xlink", XLN[1:-1])

root = ET.Element(SVG+"svg", {"height": "2000", "width": "2000",
        "viewBox": "-1000 -1000 2000 2000"})

schwarz = [pdc.apothem(8,3), mp.rect(pdc.circumradius(8,3), mp.pi/8), 0]
codes = ["", "2", "28", "2833", "3", "323"]

R = mp.fabs(pdm.rotatepi(0, 0.5, pdc.side(3,8)))
bound_data = pdm.render_polygon([R*w for w in mp.unitroots(16, primitive=True)])
root.append(ET.Element(SVG+"path", {"d": bound_data, "fill": "#fff"}))

triangles = []
for code in codes:
    v2, v3, v8 = schwarz
    for c in code:
        if c == "2":
            v3 = pdm.rotatepi(v3, 1, v2)
            v8 = pdm.rotatepi(v8, 1, v2)
        if c == "3":
            v2 = pdm.rotatepi(v2, F(2,3), v3)
            v8 = pdm.rotatepi(v8, F(2,3), v3)
        if c == "8":
            v2 = pdm.rotatepi(v2, 0.25, v8)
            v3 = pdm.rotatepi(v3, 0.25, v8)
    triangles.append(pdm.render_polygon([v2, v3, v8]))
triangledata = "".join(triangles)

root.append(ET.Element(SVG+"path", {"d": triangledata,
        "fill": "#ccc", "id": "A"}))
root.extend(pdm.draw_clones("A", 8))
root.append(ET.Element(SVG+"path", {"d": bound_data,
        "fill": "none", "stroke": "#000", "stroke-width": "10"}))

text_node = ET.Element(SVG+"text",
        {"font-family": "DejaVu Sans Mono", "font-size": "72px"})
root.append(text_node)
c, delta = -710j, -24+26j
for i in range(8):
    w = mp.root(1,8,i)
    p = c*w + delta
    px = mp.nstr(p.real, 11)
    py = mp.nstr(p.imag, 11)
    s = str((i % 4) + 1)
    el = ET.Element(SVG+"tspan", {"x": px, "y": py})
    el.text = s
    text_node.append(el)

pdm.write_node(root, f"bolza-fundamental.svg")
