#!/usr/bin/env python3.7
from xml.dom import minidom
import xml.etree.ElementTree as ET
from mpmath import mp
mp.dps = 40
F = mp.fraction
import poincaredisk as pdm
import pdmconst as pdc

SVG = "{http://www.w3.org/2000/svg}"
XLN = "{http://www.w3.org/1999/xlink}"
ET.register_namespace("", SVG[1:-1])
ET.register_namespace("xlink", XLN[1:-1])

root = ET.Element(SVG+"svg", {"height": "2000", "width": "2000",
        "viewBox": "-1000 -1000 2000 2000"})

schwarz = [pdc.apothem(7,3), mp.rect(pdc.circumradius(7,3), mp.pi/7), 0]
codes = ["", "2", "27", "277", "2777", "2772",
         "2773", "27732", "277323", "27732323", "27772", "27773",
         "2777323", "3", "323", "3232", "32323", "32327",
         "323277", "32327733", "323233", "323232", "277732", "27773233"]

i1 = pdm.rotatepi(0, F(6,7), pdc.side(3,7))
R = pdm.rotatepi(pdc.side(3,7), -F(4,7), i1).real
bound_data = pdm.render_polygon([R*w for w in mp.unitroots(14)])
root.append(ET.Element(SVG+"path", {"d": bound_data, "fill": "#fff"}))

triangles = []
for code in codes:
    v2, v3, v7 = schwarz
    for c in code:
        if c == "2":
            v3 = pdm.rotatepi(v3, 1, v2)
            v7 = pdm.rotatepi(v7, 1, v2)
        if c == "3":
            v2 = pdm.rotatepi(v2, F(2,3), v3)
            v7 = pdm.rotatepi(v7, F(2,3), v3)
        if c == "7":
            v2 = pdm.rotatepi(v2, F(2,7), v7)
            v3 = pdm.rotatepi(v3, F(2,7), v7)
    triangles.append(pdm.render_polygon([v2, v3, v7]))
triangledata = "".join(triangles)

root.append(ET.Element(SVG+"path", {"d": triangledata,
        "fill": "#ccc", "id": "A"}))
root.extend(pdm.draw_clones("A", 7))
root.append(ET.Element(SVG+"path", {"d": bound_data,
        "fill": "none", "stroke": "#000", "stroke-width": "10"}))

text_node = ET.Element(SVG+"text",
        {"font-family": "DejaVu Sans Mono", "font-size": "72px"})
root.append(text_node)
c1, delta = -96-876j, -24+26j
c2 = mp.conj(c1) * mp.root(1,7,6)
for i in range(7):
    w = mp.root(1,7,i)
    p1 = c1*w + delta
    p2 = c2*w + delta
    p1x = mp.nstr(p1.real, 11)
    p1y = mp.nstr(p1.imag, 11)
    p2x = mp.nstr(p2.real, 11)
    p2y = mp.nstr(p2.imag, 11)
    s = str(i+1)
    e1 = ET.Element(SVG+"tspan", {"x": p1x, "y": p1y})
    e2 = ET.Element(SVG+"tspan", {"x": p2x, "y": p2y})
    e1.text = s
    e2.text = s
    text_node.append(e1)
    text_node.append(e2)

pdm.write_node(root, f"klein-fundamental.svg")
