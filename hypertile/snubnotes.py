#!/usr/bin/env python3.7
from mpmath import *
import poincaredisk as pdm
mp.dps = 40

# z = r(c+si) is the magic constant for the snub triheptagonal tiling.
# If one heptagon centre is at the origin of the Poincaré disk model and a nearest heptagon's centre
# is on the +x-axis, z is the location of one of the central heptagon's vertices.
z_poly = [1, -50, 1169, -16499, 239008, -2694153, 24863475, -206377225, 1485167808, -6508039979, 35118163724, -24169525618, 80094760445, 767037157978, 2388530378972, -3693541050475, 605705719934, -20609767561411, 56156494913953, -20609767561411, 605705719934, -3693541050475, 2388530378972, 767037157978, 80094760445, -24169525618, 35118163724, -6508039979, 1485167808, -206377225, 24863475, -2694153, 239008, -16499, 1169, -50, 1]
r_poly = [1, 3, -351, -4652, -23470, -57232, -83545, -108733, -139901, -162713, -139901, -108733, -83545, -57232, -23470, -4652, -351, 3, 1]
c_poly = [262144, -2359296, 9093120, -18550784, 22199296, -16489984, 7786688, -2302672, 390392, -28561]
s_poly = [262144, 0, -344064, -1060864, 2759680, -2383360, 962752, -181104, 13720, -343]

print("z =", sqrt(findroot(lambda x: polyval(z_poly, x), 0.036+0.02j)))
print("r =", sqrt(findroot(lambda x: polyval(r_poly, x), 0.04)))
print("c =", sqrt(findroot(lambda x: polyval(c_poly, x), 0.9)))
print("s =", sqrt(findroot(lambda x: polyval(s_poly, x), 0.05)))

cz = ((0, 0, 1), (-48, -28, 8), (1308, 480, -124), (-16832, -6388, 2353),
      (148676, 59918, -20569), (-1129840, -449500, 158634),
      (5056772, 2034978, -695504))
print("z =", pdm.snubroots(cz, -1, -1))

cr = ((0, 0, 1), (-40, -14, 20), (-52, 24, 67), (-184, 22, 107))
print("r =", pdm.snubroots(cr, 0, -1))

cc = ((0, 0, 16), (-64, -32, -16), (192, 80, -15), (-121, -48, 15))
print("c =", pdm.snubroots(cc, 0, 0))

cs = ((0, 0, 16), (64, 32, -32), (64, 16, 1), (-7, 0, 0))
print("s =", pdm.snubroots(cs, 0, 0))

def palindromic_compress(p, var):
    D = Poly(p).degree(var)
    L = []
    if D & 1:
        raise ValueError("cannot compress the polynomial")
    p /= var**(D//2)
    p = p.expand().collect(var)
    for k in range(D//2,0,-1):
        C = p.coeff(var**k)
        L.append(C)
        p -= C*(var+1/var)**k
        p = p.expand().collect(var)
    L.append(p)
    return L
