from collections import deque
from xml.dom import minidom
import xml.etree.ElementTree as ET

from mpmath import mp
mp.dps = 40
F = mp.fraction

import pdmconst as pdc

SVG = "{http://www.w3.org/2000/svg}"
XLN = "{http://www.w3.org/1999/xlink}"
ET.register_namespace("", SVG[1:-1])
ET.register_namespace("xlink", XLN[1:-1])

def translate(z, t):
    """Translate point z by the vector t."""
    return (z+t)/(mp.conj(t)*z + 1)

# This can also be expressed as 1/t * A/conj(A) * translate(z, D)
# where t = expjpi(k), A = c*conj(c) - t, D = c*(t-1) / A,
# but that would be overkill.
def rotatepi(z, k, c=0):
    """Rotate point z by angle k*pi around point c."""
    return translate(translate(z, -c) * mp.expjpi(k), c)

def make_hep_graph(limit=0.999):
    # Each entry in the queue stores (parent, point, edge type)
    queue = [(0, pdc.side(3,7), 1)]
    tree, holes, other = [], [], []
    while queue:
        p, z, t = queue.pop()
        if abs(z) > limit:
            continue
        queue.append((z, rotatepi(p, F(6,7), z), 1))
        tree.append((p, z))
        holes.append((z, rotatepi(p, F(4,7), z)))
        if t == 1:
            queue.append((z, rotatepi(p, -F(6,7), z), 1))
            queue.append((z, rotatepi(p, -F(4,7), z), 2))
            other.append((z, rotatepi(p, -F(2,7), z)))
        else:
            queue.append((z, rotatepi(p, -F(6,7), z), 2))
            other.append((z, rotatepi(p, -F(4,7), z)))
    return (tree, holes, other)

def make_pent_graph(limit=0.999):
    queue = [(0, pdc.side(4,5), 11)]
    g1, g2, g3 = [], [], []
    while queue:
        p, z, t = queue.pop()
        if abs(z) > limit:
            continue
        if t == 12:
            g2.append((p, z))
        else:
            g1.append((p, z))
        g3.append((z, rotatepi(p, F(2,5), z)))
        if t % 10 == 1:
            queue.append((z, rotatepi(p, F(4,5), z), 11))
            queue.append((z, rotatepi(p, -F(4,5), z), 11))
            queue.append((z, rotatepi(p, -F(2,5), z), 12))
        if t % 10 == 2:
            queue.append((z, rotatepi(p, F(4,5), z), 21))
            queue.append((z, rotatepi(p, -F(4,5), z), 22))
    return (g1+g2+g3, g1, g1+g2)

def make_oct_graph(limit=0.999):
    queue = [(0, pdc.side(3,8), 1)]
    g1, g2, g3 = [], [], []
    while queue:
        p, z, t = queue.pop()
        if abs(z) > limit:
            continue
        g1.append((p, z))
        g3.append((z, rotatepi(p, -0.25, z)))
        if t == 1:
            queue.append((z, rotatepi(p, 0.75, z), 1))
            queue.append((z, rotatepi(p, 1, z), 1))
            queue.append((z, rotatepi(p, -0.75, z), 1))
            queue.append((z, rotatepi(p, -0.5, z), 2))
        if t == 2:
            queue.append((z, rotatepi(p, 0.75, z), 1))
            queue.append((z, rotatepi(p, 1, z), 1))
            queue.append((z, rotatepi(p, -0.75, z), 2))
            g2.append((z, rotatepi(p, -0.5, z)))
    return (g1+g2+g3, g1+g2, g1)

def point_coincidence(l, p):
    min_dist = min((abs(p - v) for v in l), default=2)
    return min_dist < 1e-10

def make_general_graph(p, q, limit=0.999):
    # p >= 5
    dq = deque([(0, pdc.side(q,p))])
    a_lower, a_upper = pdc.bounding_angles(p,q)
    edges, vertices = [], []
    discovered = []
    while dq:
        parent, child = dq.popleft()
        print(abs(child))
        edges.append((parent, child))
        vertices.append((parent, child))
        discovered.append(child)
        if abs(child) > limit:
            continue
        for i in range(1,p):
            grandchild = rotatepi(parent, -F(2*i,p), child)
            if point_coincidence(discovered, grandchild) or mp.arg(grandchild) < a_lower:
                continue
            qp = [p[1] for p in dq]
            if point_coincidence(qp, grandchild) or mp.arg(grandchild) > a_upper:
                edges.append((child, grandchild))
            else:
                dq.append((child, grandchild))
    return (edges, [], vertices)

def norm(z):
    """Return the norm of the complex number z
    (squared absolute value). Equivalent to z*mp.conj(z)."""
    return z.real*z.real + z.imag*z.imag

def hypermetric(u, v):
    """Compute a hyperbolic metric between u and v."""
    return norm(u-v) / ((1-norm(u))*(1-norm(v)))

def hyperline(a, b):
    """Return (centre, radius) of the hyperbolic straight line
    between a and b, distorted into a circle."""
    numerator = (a*(norm(b) + 1) - b*(norm(a) + 1))
    c = numerator / (a*mp.conj(b) - b*mp.conj(a))
    return (c, mp.sqrt(norm(c) - 1))

def render_segment(a, b, scale=1000):
    """Draw the hyperbolic straight line from a to b at the given scale.
    Return a tuple of the head (a) and tail (b) SVG path data strings."""
    ax = round(a.real * scale, 8)
    ay = round(a.imag * scale, 8)
    head = f"M{ax} {ay}"
    bx = round(b.real * scale, 8)
    by = round(b.imag * scale, 8)
    cross = a.real*b.imag - a.imag*b.real
    if abs(cross) < 1e-20 * scale:
        return (head, f"L{bx} {by}")
    r = round(hyperline(a, b)[1] * scale, 8)
    sweep = 0 if cross > 0 else 1
    return (head, f"A{r} {r} 0 0 {sweep} {bx} {by}")

def thick_segment(a, b, width=0.012, scale=1000, curvedends=False):
    """Draw a "thick" line segment from a to b at the given scale.
    The width is half of the Euclidean distance that would be measured
    if one endpoint of the segment was placed at the disk's origin."""
    # Place a at the origin, get p1 and p2
    s_b = mp.sign(translate(b, -a)) * width * 1j
    p1 = translate(s_b, a)
    p2 = translate(-s_b, a)
    # Place b at the origin, get p3 and p4
    s_a = mp.sign(translate(a, -b)) * width * 1j
    p3 = translate(s_a, b)
    p4 = translate(-s_a, b)
    if curvedends:
        return render_polygon((p1, p4, p3, p2))
    head1, tail1 = render_segment(p1, p4, scale)
    head2, tail2 = render_segment(p3, p2, scale)
    return head1 + tail1 + "L" + head2[1:] + tail2 + "Z"

def patch(template, edges):
    """Generate copies of the template, a list of points, so that
    (template copy + any edge) is congruent to (template + first edge)
    with the same orientation."""
    # The normalised template's edge points in the +x direction from origin
    yield template
    v = mp.sign(translate(edges[0][1], -edges[0][0]))
    normal_template = [translate(p, -edges[0][0]) / v for p in template]
    for edge in edges[1:]:
        v = mp.sign(translate(edge[1], -edge[0]))
        yield [translate(p*v, edge[0]) for p in normal_template]

def render_polygon(points, scale=1000):
    """Draw the closed polygon with the given points and scale factor.
    Return corresponding SVG path data."""
    strings = [render_segment(points[i-1], points[i], scale)
            for i in range(len(points))]
    return strings[0][0] + "".join(pair[1] for pair in strings) + "Z"

# The following functions construct and manipulate SVG elements.

def make_root_circle(fill):
    """Initialise an SVG tree with a boundary circle of the
    specified fill."""
    root = ET.Element(SVG+"svg", {"height": "2000", "width": "2000",
        "viewBox": "-1000 -1000 2000 2000"})
    circ = ET.Element(SVG+"circle", {"r": "1000", "fill": fill})
    root.append(circ)
    return root

def draw_path(d, fill, name=None):
    """Construct an SVG path element with the given data, name and fill.
    name may be left blank."""
    attribs = {"d": d, "fill": fill}
    if name is not None:
        attribs["id"] = name
    return ET.Element(SVG+"path", attribs)

def draw_clones(id_, N):
    """Return a list of N-1 clones of the element with the given ID,
    spaced evenly."""
    rotations = [f"rotate({360*i/N})" for i in range(1, N)]
    return [ET.Element(SVG+"use",
            {XLN+"href": "#"+id_, "transform": t}) for t in rotations]

def write_node(node, filename):
    """Write the given node to the given filename."""
    out = minidom.parseString(ET.tostring(node))
    out = out.toprettyxml(indent="  ").partition("\n")[2]
    with open(filename, 'w') as f:
        f.write(out)
