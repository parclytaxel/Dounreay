#!/usr/bin/env python3.7
from xml.dom import minidom
import xml.etree.ElementTree as ET
from mpmath import mp
mp.dps = 40
F = mp.fraction
import poincaredisk as pdm
import pdmconst as pdc

SVG = "{http://www.w3.org/2000/svg}"
XLN = "{http://www.w3.org/1999/xlink}"
ET.register_namespace("", SVG[1:-1])
ET.register_namespace("xlink", XLN[1:-1])

root = ET.Element(SVG+"svg", {"height": "2000", "width": "2000",
        "viewBox": "-1000 -1000 2000 2000"})

schwarz = [pdc.apothem(5,4), mp.rect(pdc.circumradius(5,4), mp.pi/5), 0]
codes = ["2", "25", "252", "254", "255", "2552",
         "2542", "2544", "2554", "25525", "25424", "255254",
         "", "4", "42", "424", "425", "4242",
         "4244", "4252", "25544", "42442", "42552", "4244252"]

R1 = pdm.rotatepi(mp.rect(pdc.side(4,5), 2*mp.pi/5), -F(4,5), pdc.side(4,5)).real
i1 = pdm.rotatepi(0, -F(4,5), pdc.side(4,5))
R2 = pdm.rotatepi(mp.conj(i1), -F(4,5), i1)
bound_data = pdm.render_polygon([R*w for w in mp.unitroots(10) for R in (R1,R2)])
root.append(ET.Element(SVG+"path", {"d": bound_data, "fill": "#fff"}))

triangles = []
for code in codes:
    v2, v4, v5 = schwarz
    for c in code:
        if c == "2":
            v4 = pdm.rotatepi(v4, 1, v2)
            v5 = pdm.rotatepi(v5, 1, v2)
        if c == "4":
            v2 = pdm.rotatepi(v2, 0.5, v4)
            v5 = pdm.rotatepi(v5, 0.5, v4)
        if c == "5":
            v2 = pdm.rotatepi(v2, F(2,5), v5)
            v4 = pdm.rotatepi(v4, F(2,5), v5)
    triangles.append(pdm.render_polygon([v2, v4, v5]))
triangledata = "".join(triangles)

root.append(ET.Element(SVG+"path", {"d": triangledata,
        "fill": "#ccc", "id": "A"}))
root.extend(pdm.draw_clones("A", 5))
root.append(ET.Element(SVG+"path", {"d": bound_data,
        "fill": "none", "stroke": "#000", "stroke-width": "10"}))

seq1 = "0915263748"
seq2 = "5364708192"
text_node = ET.Element(SVG+"text",
        {"font-family": "DejaVu Sans Mono", "font-size": "72px"})
root.append(text_node)
c1, delta = -64-936j, -24+26j
c2 = 69-936j
for i in range(10):
    w = mp.root(1,10,-i)
    p1 = c1*w + delta
    p2 = c2*w + delta
    p1x = mp.nstr(p1.real, 11)
    p1y = mp.nstr(p1.imag, 11)
    p2x = mp.nstr(p2.real, 11)
    p2y = mp.nstr(p2.imag, 11)
    e1 = ET.Element(SVG+"tspan", {"x": p1x, "y": p1y})
    e2 = ET.Element(SVG+"tspan", {"x": p2x, "y": p2y})
    e1.text = seq1[i]
    e2.text = seq2[i]
    text_node.append(e1)
    text_node.append(e2)

pdm.write_node(root, f"bring-fundamental.svg")
