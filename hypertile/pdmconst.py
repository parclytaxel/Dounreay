#!/usr/bin/env python3.7
from mpmath import mp
mp.dps = 40
F = mp.fraction
sp = lambda a, b: mp.sinpi(F(a,b))
cp = lambda a, b: mp.cospi(F(a,b))

# Magic constant generator for regular and uniform tilings of
# the hyperbolic plane. These are only valid if 1/p + 1/q < 1/2.
# {p,q} means the regular tiling of p-gons, q around a vertex.

# The Euclidean or E-distance is the length of a hyperbolic line segment
# when one end lies at the origin. It is related to the hyperbolic or
# H-distance by x_H = log((1+x_E)/(1-x_E)).

# By convention, when presenting hyperbolic tilings, the diagram has
# max(p,q)-fold symmetry around the disc centre, but the formulas below
# work in the dual case as well.

def side(p, q):
    """E-distance of sides of {p,q}.
    Magic constant when p < q."""
    return mp.sqrt(1 - (sp(1,q)/cp(1,p))**2)

def bounding_angles(p, q):
    """Assuming p >= 5, return [lower, upper] angle bounds of
    the search region for the general tree generator."""
    k = (p-1) // 2
    theta = mp.acos(cp(k,p) / side(q,p))
    return (theta - k*mp.pi/p, theta - (k-2)*mp.pi/p)

def circumradius(p, q):
    """E-distance of polygon circumradius in {p,q}.
    Magic constant when p > q."""
    return mp.sqrt(cp(p+q,p*q) / cp(p-q,p*q))

def apothem(p, q):
    """E-distance of polygon apothem in {p,q}.
    Magic constant for r{p,q} if centred on a p-gon."""
    return mp.sqrt((cp(1,q)-sp(1,p)) / (cp(1,q)+sp(1,p)))

# For a hyperbolic triangle with one side c_H and adjacent angles A and B,
# cos(C) = cosh(c_H)*sin(a)*sin(b) - cos(a)*cos(b)
# and sinh(b_H) = sin(B)*sinh(c_H) / sin(C).
# These are used to derive the formulas below; f = sinh^2(v_H)
# where v is the value in question.

def truncated_2p_radius(p, q):
    """E-distance of 2p-gon circumradius in t{p,q}.
    Magic constant when p > q."""
    f = (sp(1,p)**2 - cp(1,q)**2) / ((cp(1,q)*sp(1,2*p))**2 - sp(1,p)**2)
    return (mp.sqrt(f+1) - 1) / mp.sqrt(f)

def truncated_q_radius(p, q):
    """E-distance of q-gon circumradius in t{p,q}.
    Magic constant when p < q."""
    f = ((cp(1,p)/sp(1,q))**2 - 1) / (sp(1,q)**2 + 2*cp(1,p) + 1)
    return (mp.sqrt(f+1) - 1) / mp.sqrt(f)

def cantellated_p_radius(p, q):
    """E-distance/magic constant of p-gon circumradius in rr{p,q}."""
    f = (1 - (cp(1,q)/sp(1,p))**2) / ((cp(1,p)-cp(1,q))**2 - 2)
    return (mp.sqrt(f+1) - 1) / mp.sqrt(f)

def omnitruncated_p_radius(p, q):
    """E-distance/magic constant of p-gon circumradius in tr{p,q}."""
    f = (sp(1,p)**2 - cp(1,q)**2) / (((cp(1,p)-cp(1,q)+1)*sp(1,2*p))**2 - 2*sp(1,p)**2)
    return (mp.sqrt(f+1) - 1) / mp.sqrt(f)

# The snub tilings require very careful treatment.

def poly_newton(coeffs, x0):
    """Find a root of the polynomial defined by coeffs
    using Newton's method starting from x0."""
    x, delta, tol = x0, 1, mp.mpf(f"1e-{mp.dps-10}")
    while abs(delta) > tol:
        Lx, Dx = mp.polyval(coeffs, x, derivative=True)
        delta = Lx / Dx
        x -= delta
    return x

def snub_p_vector(p, q):
    """Magic constant (a complex number) of the position vector of
    one point of a p-gon in sr{p,q}."""
    with mp.extradps(20):
        a = sp(1,p)**2
        b = cp(1,q)**2
        L = [(a-b)**2,
             8*(a-b)*(2*a**2-2*a*b-a+2*b),
             8*(12*a**4+a**3-a*b - (3*a*b+2*a-3*b)*(6*a**2-4*a*b-a+4*b)),
             32*(a**3*(4*a-3)*(2*a-1) - 2*b*(a-1)**2*(a**2-4*a*b-2*a+4*b)),
             16*(a**2*(4*a-3) + 4*b*(a-1)**2)**2]
        r = poly_newton(L, 1+max(-L[k]/L[0] for k in range(1,5)))
        R = (mp.sqrt(r) - mp.sqrt(r-4)) / 2

        sth = sp(1,p)/mp.sqrt(2)
        if p != q:
            L = [(a-b)**2,
                 4*(a-b)*(2*b-1),
                 -2*(6*a*b-a-12*b**2+7*b-2),
                 4*(2*a*b-8*b**2+4*b-1),
                 (4*b-1)**2]
            f = poly_newton(L, 1)
            sth *= mp.sqrt(f)
        cth = mp.sqrt(1-sth*sth)
        return mp.mpc(R*cth, R*sth)

def floret_constant(p,q):
    if p == q:
        r = 3/cp(2,p)+1
    else:
        with mp.extradps(40):
            a = sp(1,p)**2
            b = cp(1,q)**2
            L = [((a-b)*(4*a-3))**2,
                 -8*(a-b)*(8*a**3 - 26*a**2*b - 18*a**2 + 42*a*b + 9*a - 18*b),
                 24*(4*a**4 - 26*a**3*b - 9*a**3 + 40*a**2*b**2 + 63*a**2*b + 6*a**2 - 72*a*b**2 - 36*a*b + 36*b**2),
                 -32*(2*a**4 - 4*a**3*b - 3*a**3 + 56*a**2*b**2 + 42*a**2*b - 120*a*b**2 - 36*a*b + 72*b**2),
                 16*(a**2+8*a*b-12*b)**2]
            r = poly_newton(L, 1+max(-L[k]/L[0] for k in range(1,5)))
    return (mp.sqrt(r) - mp.sqrt(r-4)) / 2
