\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{url}
\usepackage[left=2cm, right=2cm, top=2cm, bottom=2cm]{geometry}

%opening
\title{Elliptic Integrals and Functions}
\author{Jeremy Tan}

\begin{document}

\maketitle

\begin{abstract}
This is a collection of various results pertaining to elliptic integrals and Jacobi elliptic functions which I have found useful while answering questions on the Mathematics Stack Exchange. Many times someone puts up an integral that \textit{seems} simple enough, but can only be solved with elliptic integrals -- this collection should make their problem a little easier.\footnote{This work is licenced under CC BY-SA 4.0.}
\end{abstract}

\section{Definitions}

Elliptic integrals and functions are notorious for being defined in several ways, with the transformations between those definitions not always obvious. Here I use the definitions implied by their implementations in Mathematica (i.e. the Wolfram Language) and the Python library \textsf{mpmath}:\vspace{3mm}

\textit{First-kind elliptic integral}
\begin{equation*}
F(\varphi,m)=\int_0^\varphi\frac1{\sqrt{1-m\sin^2\theta}}\,d\theta\qquad K(m)=F(\pi/2,m)
\end{equation*}

\textit{Second-kind elliptic integral}
\begin{equation*}
E(\varphi,m)=\int_0^\varphi\sqrt{1-m\sin^2\theta}\,d\theta\qquad E(m)=E(\pi/2,m)
\end{equation*}

\textit{Third-kind elliptic integral}
\begin{equation*}
\Pi(n,\varphi,m)=\int_0^\varphi\frac1{(1-n\sin^2\theta)\sqrt{1-m\sin^2\theta}}\,d\theta\qquad\Pi(n,m)=\Pi(n,\pi/2,m)
\end{equation*}

\textit{Jacobi elliptic functions}
\newcommand{\am}{\operatorname{am}}
\newcommand{\sn}{\operatorname{sn}}
\newcommand{\cn}{\operatorname{cn}}
\newcommand{\dn}{\operatorname{dn}}
\newcommand{\ns}{\operatorname{ns}}
\newcommand{\nc}{\operatorname{nc}}
\newcommand{\nd}{\operatorname{nd}}
\newcommand{\tn}{\operatorname{tn}}
\newcommand{\sd}{\operatorname{sd}}
\newcommand{\cd}{\operatorname{cd}}
\newcommand{\cs}{\operatorname{cs}}
\newcommand{\nt}{\operatorname{nt}}
\newcommand{\ds}{\operatorname{ds}}
\newcommand{\dc}{\operatorname{dc}}
\begin{equation*}
F(\varphi,m)=u\iff\begin{cases}
\varphi=\am(u,m)\\
\sin\varphi=\sn(u,m)\\
\cos\varphi=\cn(u,m)\\
\sqrt{1-m\sin^2\varphi}=\dn(u,m)
\end{cases}
\end{equation*}

The last argument of elliptic functions (the parameter) is suppressed when clear from context, so $\sn u$, $\cn u$ and so on. The other nine elliptic functions are ratios of $\sn$, $\cn$, $\dn$ and the constant-1 $\operatorname{nn}$; they are denoted by placing the first letters of the numerator and then the denominator together, e.g. $\sd u=\frac{\sn u}{\dn u}$.

Where BF followed by a numeric identifier is mentioned, this refers to the corresponding entry in Byrd and Friedman's \textit{Handbook of Elliptic Integrals for Engineers and Physicists}. The notation $\tn u$ is carried over from that book, equivalent to $\operatorname{sc}u=\tan\varphi$, and here I introduce $\nt u=\cs u=\cot\varphi$. Where DLMF followed by a numeric identifier is mentioned, this refers to the corresponding entry in the Digital Library of Mathematical Functions. Both these references use $k$ as the last argument in their definitions of elliptic integrals and functions; their $k$ is my $\sqrt m$, my $m$ is their $k^2$.

\subsection{In the complex plane}

In 1954, when Byrd and Friedman came out, it was prudent to avoid complex numbers in calculations because special functions were most commonly read off from tables and complex numbers meant double the work. Yet with native or library support for complex numbers in virtually all programming languages, as well as transformations that go out of the real line, it is necessary to clarify how the definitions above extend to the complex plane.

\textsf{mpmath} provides the following guidance: ``In the defining integral, it is assumed that the principal branch of the square root is taken and that the path of integration avoids crossing any branch cuts.'' This implies that when $m$ is fixed,

\begin{itemize}
	\item the three elliptic integrals have branch points at $1-m\sin^2\theta=0$ or $\theta=\pm\sin^{-1}\frac1{\sqrt m}$ and branch cuts at $\theta=\pm\sin^{-1}\sqrt{\frac xm}$ where $x>1$ (all branches of $\sin^{-1}$ are taken into account)
	\item if $1-m\sin^2\theta$ is exactly a negative real number, the interpreted value of the integrand has \textit{negative} imaginary part for $F$ and $\Pi$ and \textit{positive} imaginary part for $E$, e.g. $K(2)$ has negative imaginary part
\end{itemize}

\section{Transformations of arguments}

\subsection{Periodicity}

Let $a$ be an integer, then (BF 113.02, DLMF 19.2.10)
\begin{align*}
F(a\pi\pm\varphi,m)&=2aK(m)\pm F(\varphi,m)\\
E(a\pi\pm\varphi,m)&=2aE(m)\pm E(\varphi,m)\\
\Pi(n,a\pi\pm\varphi,m)&=2a\Pi(n,m)\pm\Pi(n,\varphi,m)
\end{align*}
These allow reduction of $\operatorname{Re}(\varphi)$ to $[-\pi/2,\pi/2]$.

Let $a,b$ be integers and $q_1=K(m),q_2=iK(1-m)$ be quarter periods, then (BF 122.03, .07, .25, DLMF 22.4(iii))
\begin{equation*}
\begin{array}{lll}
\sn(u+2aq_1+2bq_2)=(-1)^a\sn u&\sn(u+q_1)=\cd u&\sn(u+q_2)=\frac1{\sqrt m}\ns u\\
\cn(u+2aq_1+2bq_2)=(-1)^{a+b}\cn u&\cn(u+q_1)=-\sqrt{1-m}\sd u&\cn(u+q_2)=\frac{-i}{\sqrt m}\ds u\\
\dn(u+2aq_1+2bq_2)=(-1)^b\dn u&\dn(u+q_1)=\sqrt{1-m}\nd u&\dn(u+q_2)=-i\cs u
\end{array}
\end{equation*}
These allow reduction of $u$ to a parallelogram defined in the complex plane by $q_1$ and $q_2$. One curious implication of the elliptic functions' double periodicity is the integrals' multivaluedness in the most general case: if an elliptic integral evaluates to $u$ it could also evaluate to $2K(m)-u$, $u+2iK(1-m)$ and $u+4K(m)$.

\subsection{Zero and one arguments}

In the classical method of evaluating elliptic integrals and functions using Landen transformations, the parameter is eventually transformed to some number close to zero or one, whereby the following reductions finish the computation (BF 111.01, .04, 122.08, .09, DLMF 19.6.7, .9, .12, 22.5(ii)):
\begin{equation*}
\begin{array}{ll}
F(\varphi,0)=\varphi&F(\varphi,1)=\tanh^{-1}\sin\varphi\\
E(\varphi,0)=\varphi&E(\varphi,1)=\sin\varphi\\
\Pi(n,\varphi,0)=\frac1{\sqrt{n-1}}\tanh^{-1}(\sqrt{n-1}\tan\varphi)&\Pi(n,\varphi,1)=\frac1{n-1}(\sqrt n\tanh^{-1}(\sqrt n\sin\varphi)-\tanh^{-1}\sin\varphi)\\
\sn(u,0)=\sin u&\sn(u,1)=\tanh u\\
\cn(u,0)=\cos u&\cn(u,1)=\operatorname{sech} u\\
\dn(u,0)=1&\dn(u,1)=\operatorname{sech} u
\end{array}
\end{equation*}

There are also the following simplifications for special values of the $n$ argument to $\Pi$ (BF 111.06, DLMF 19.6.13):
\begin{align*}
\Pi(0,\varphi,m)&=F(\varphi,m)=u\\
\Pi(1,\varphi,m)&=F(\varphi,m)-\frac{E(\varphi,m)-\dn u\tn u}{1-m}\\
\Pi(m,\varphi,m)&=\frac{E(\varphi,m)-m\sd u\cn u}{1-m}
\end{align*}

\subsection{Linear fractional transformations of the parameter}

Let $m^*=\frac1m$ and $\psi=\sin^{-1}(\sqrt m\sin\varphi)$, then (BF 162.01, .02, DLMF 19.7.4, 22.17.2, .3, .4)
\begin{align*}
F(\varphi,m)&=\sqrt{m^*}F(\psi,m^*)&\sn(u,m)&=\sn(u\sqrt m,m^*)\sqrt{m^*}\\
E(\varphi,m)&=\sqrt m(E(\psi,m^*)-(1-m^*)F(\psi,m^*))&\cn(u,m)&=\dn(u\sqrt m,m^*)\\
\Pi(n,\varphi,m)&=\sqrt{m^*}\Pi(nm^*,\psi,m^*)&\dn(u,m)&=\cn(u\sqrt m,m^*)
\end{align*}

Let $m^*=\frac m{m-1}$ and $\sin\psi=\frac{\sqrt{1-m}\sin\varphi}{\sqrt{1-m\sin^2\varphi}}=\sn(u,m^*)$, then (BF 160.01, .02, DLMF 19.7.5, 22.17.6, .7, .8)
\begin{align*}
F(\varphi,m)&=\sqrt{1-m^*}F(\psi,m^*)&\sn(u,m)&=\sd(u\sqrt{1-m},m^*)\sqrt{1-m^*}\\
E(\varphi,m)&=\sqrt{1-m}(E(\psi,m^*)-m^*\sd u\cn u)&\cn(u,m)&=\cd(u\sqrt{1-m},m^*)\\
\Pi(n,\varphi,m)&=\frac{\sqrt{1-m^*}}{m-n}\left(mF(\psi,m^*)-n\Pi\left(\frac{m-n}{m-1},\psi,m^*\right)\right)&\dn(u,m)&=\nd(u\sqrt{1-m},m^*)
\end{align*}

The set of parameters $m$ that can be reached with these transformations alone forms the \textit{anharmonic group}, isomorphic to the symmetric group $S_3$. In particular, let $m^*=1-m$ and $\tan\psi=-i\sin\varphi=\tn(u,m^*)$, then (BF 161.01, .02, DLMF 19.7.7, 22.6(iv))
\begin{align*}
F(\varphi,m)&=iF(\psi,m^*)&\sn(u,m)&=-i\tn(iu,m^*)\\
E(\varphi,m)&=i(F(\psi,m^*)-E(\psi,m^*)+\tn u\dn u)&\cn(u,m)&=\nc(iu,m^*)\\
\Pi(n,\varphi,m)&=\frac i{1-n}(F(\psi,m^*)-n\Pi(1-n,\psi,m^*))&\dn(u,m)&=\dc(iu,m^*)
\end{align*}

\subsection{Landen and Gauss transformations}

\textit{Descending Landen}: Let $k^*=\frac{1-\sqrt{1-m}}{1+\sqrt{1-m}}$, $m^*=(k^*)^2$, $\psi=\varphi+\tan^{-1}(\sqrt{1-m}\tan\varphi)$ and $u^*=\frac u{1+k^*}$, then (BF 163.02, 164.01, DLMF 19.8.13, .14, 22.7(i))
\begin{align*}
F(\varphi,m)&=\frac{1+k^*}2F(\psi,m^*)\\
E(\varphi,m)&=\frac1{1+k^*}E(\psi,m^*)-\frac{1-k^*}2F(\psi,m^*)+\frac{k^*}{1+k^*}\sin\psi\\
\Pi(n,\varphi,m)&=\frac1{m-n}\left((\omega-n)\frac{1+k^*}4\Pi(n^*,\psi,m^*) + \frac{k^*}{1+k^*}F(\psi,m^*) - \frac{\sqrt{n^*}}{1+k^*}\tanh^{-1}(\sqrt{n^*}\sin\psi)\right)\\
&\left[\omega=\frac{m-n}{1-n},n^*=\frac{n\omega}4(1+k^*)^2\right]
\end{align*}
\begin{align*}
\sn(u,m)&=\frac{(1+k^*)\sn(u^*,m^*)}{1+k^*\sn^2(u^*,m^*)}\\
\cn(u,m)&=\frac{\cn(u^*,m^*)\dn(u^*,m^*)}{1+k^*\sn^2(u^*,m^*)}\\
\dn(u,m)&=\frac{1-k^*\sn^2(u^*,m^*)}{1+k^*\sn^2(u^*,m^*)}
\end{align*}

\textit{Ascending Landen}: Let $k=\sqrt m$, $m^*=\frac{4k}{(1+k)^2}$, $2\psi=\varphi+\sin^{-1}(k\sin\varphi)$ and $u^*=\frac{1+k}2u$, then (BF 163.02, 164.01, DLMF 19.8.17, 22.7(ii))
\begin{align*}
F(\varphi,m)&=\frac2{1+k}F(\psi,m^*)\\
E(\varphi,m)&=(1+k)E(\psi,m^*)+(1-k)F(\psi,m^*)-k\sin\varphi\\
\Pi(n,\varphi,m)&=\frac4{(1+k)(\omega-n^\pm)}\left((m^*-n^\pm)\Pi(n^\pm,\psi,m^*)- \frac{m^*}2F(\psi,m^*)+\frac{\sqrt{n}}{1+k}\tanh^{-1}(\sqrt{n}\sin\varphi)\right)\\
&=\frac2{(1+k)(n^+-n^-)}((n^+-m^*)\Pi(n^+,\psi, m^*)-(n^--m^*)\Pi(n^-,\psi, m^*))\\
&\left[n^\pm=\frac{2(k+n\pm\sqrt{(n-m)(n-1)})}{(1+k)^2},\omega=\frac{m^*-n^\pm}{1-n^\pm}\right]
\end{align*}
\begin{align*}
\sn(u,m)&=\frac2{1+k}\sd(u^*,m^*)\cn(u^*,m^*)\\
\cn(u,m)&=\nd(u^*,m^*)-\frac2{1+k}\sd(u^*,m^*)\sn(u^*,m^*)\\
\dn(u,m)&=\nd(u^*,m^*)-\frac{2k}{1+k}\sd(u^*,m^*)\sn(u^*,m^*)
\end{align*}

\textit{Descending Gauss}: Let $k^*=\frac{1-\sqrt{1-m}}{1+\sqrt{1-m}}$, $m^*=(k^*)^2$, $\sin\varphi=\sn(u,m)$ and $\psi=\sin^{-1}\frac{(1+\sqrt{1-m})\sn u}{1+\dn u}$, then (BF 164.02, DLMF 19.8.19, .20)
\begin{align*}
F(\varphi,m)&=(1+k^*)F(\psi,m^*)\\
E(\varphi,m)&=\frac2{1+k^*}E(\psi,m^*)-(1-k^*)F(\psi,m^*)+(1-\dn u)\nt u\\
\Pi(n,\varphi,m)&=\frac1\omega\left(2(1+k^*)\Pi(n^*,\psi,m^*)+(\omega-1)(1+k^*)F(\psi,m^*)-\frac{\tanh^{-1}(\sqrt{n-1}\tan\varphi)}{\sqrt{n-1}}\right)\\
&\left[\omega=\sqrt{1-\frac mn},n^*=\frac n4(1+\omega)^2(1+k^*)^2\right]
\end{align*}

\textit{Ascending Gauss}: Let $k=\sqrt m$, $m^*=\frac{4k}{(1+k)^2}$, $\sin\varphi=\sn(u,m)$ and $\psi=\sin^{-1}\frac{(1+k)\sin\varphi}{1+k\sin^2\varphi}$, then (BF 164.02)
\begin{align*}
F(\varphi,m)&=\frac1{1+k}F(\psi,m^*)\\
E(\varphi,m)&=\frac{1+k}2\left(E(\psi,m^*)+\frac{1-k}{1+k}F(\psi,m^*)-\frac{2k}{1+k}\frac{\sn u\cn u\dn u}{1+k\sn^2u}\right)\\
\Pi(n,\varphi,m)&=\frac1{2(1+k)}\left(\omega\Pi(n^*,\psi,m^*)+(1-\omega)F(\psi,m^*)+\frac{\tanh^{-1}(\sqrt{n^*-1}\tan\psi)}{\sqrt{n^*-1}}\right)\\
&\left[n^*=\frac{(n+k)^2}{n(1+k)^2},\omega=\frac{n-k}{n+k}\right]
\end{align*}

The Gauss transformations send complete integrals to complete integrals. Let $k^*=\frac{1-\sqrt{1-m}}{1+\sqrt{1-m}}$ and $m^*=(k^*)^2$, then (BF 164.02, DLMF 19.8.12)
\begin{align*}
K(m)&=(1+k^*)K(m^*)\\
E(m)&=\frac2{1+k^*}E(m^*)-(1-k^*)K(m^*)
\end{align*}

Let $k=\sqrt m$ and $m^*=\frac{4k}{(1+k)^2}$, then (BF 164.02)
\begin{align*}
K(m)&=\frac1{1+k}K(m^*)\\
E(m)&=\frac{1+k}2E(m^*)+\frac{1-k}2K(m^*)
\end{align*}

\subsection{Addition formulas}

Let $F(\alpha,m)=u$ and $F(\beta,m)=v$, then (BF 123.01, DLMF 22.8.1, .2, .3)
\begin{align*}
\sin\gamma&=\sn(u+v)=\frac{\sn u\cn v\dn v+\sn v\cn u\dn u}{1-m\sn^2u\sn^2v}\\
\cos\gamma&=\cn(u+v)=\frac{\cn u\cn v-\sn u\sn v\dn u\dn v}{1-m\sn^2u\sn^2v}\\
\sqrt{1-m\sin^2\gamma}&=\dn(u+v)=\frac{\dn u\dn v-m\sn u\sn v\cn u\cn v}{1-m\sn^2u\sn^2v}
\end{align*}
Using the value of $\gamma$ above, the formulas for elliptic integrals follow (BF 116.01, .02, .03, DLMF 19.11(i)):
\begin{align*}
F(\alpha,m)+F(\beta,m)&=F(\gamma,m)\\
E(\alpha,m)+E(\beta,m)&=E(\gamma,m)+m\sin\alpha\sin\beta\sin\gamma\\
\Pi(n,\alpha,m)+\Pi(n,\beta,m)&=\Pi(n,\gamma,m)+\frac n\omega\tanh^{-1}\frac{\omega\sin\alpha\sin\beta\sin\gamma}{n(1-\cos\alpha\cos\beta\cos\gamma)-1}\\
&[\omega=\sqrt{n(n-1)(n-m)}]
\end{align*}

\subsection{Separation into real and imaginary parts}

Let $m$ be a \textbf{real} number and $u$ an arbitrary complex number. Elliptic functions of $u$ and $m$ are easily separable into real and imaginary parts:
\begin{itemize}
	\item Linear fractional transformations bring $m$ into $[0,1]$, where all elliptic functions are real for real $u$
	\item The addition formula separates functions of $u=a+bi$ into functions of $a$ and $bi$
	\item The $m^*=1-m$ transformation turns a function of $bi$ into a function of $b$
\end{itemize}

If $\varphi$ is an arbitrary complex number, splitting elliptic integrals of $\varphi$ and $m$ is a little harder. Bring $m$ into $[0,1]$ as before and $\operatorname{Re}(\varphi)$ into $[-\pi/2,\pi/2]$, then the problem is solved for any kind of integral if real $\alpha$ and $\beta$ can be found satisfying $F(\alpha,m)+F(i\beta,m)=F(\varphi,m)$; the $m^*=1-m$ transformation and the complex relations between trigonometric and hyperbolic functions take care of $i\beta$.

Write $\varphi=a+ib$ and let $c=\sin^2a$, $d=\sinh^2b$, $A=\sin^2\alpha$ and $B=\sinh^2\beta$. If $|a|<\frac\pi2$, $A$ and $B$ are the smaller roots of
\begin{equation*}
(d+1)mA^2-(1+(c+d)m)A+c\qquad\text{and}\qquad(c-1)mB^2-(1-(c+d)m)B+d
\end{equation*}

If $|a|=\frac\pi2$ then (BF 115.02, .03)
\begin{equation*}
A=\begin{cases}
1&m(d+1)\le1\\\frac 1{m(d+1)}&m(d+1)\ge1
\end{cases}\qquad B=\begin{cases}
\frac d{1-m(d+1)}&m(d+1)\le1\\\infty&m(d+1)\ge1
\end{cases}
\end{equation*}
where $F(\pm i\infty,m)=\pm iK(1-m)$. In all cases, when deriving $\alpha$ and $\beta$ from $A$ and $B$, the signs of $\alpha$ and $\beta$ match those of $a$ and $b$ respectively.

\subsection{Legendre's relation}

For any $m$, real or complex, the following identity holds (BF 110.10, DLMF 19.7.1):
\begin{equation*}
K(m)E(1-m)+K(1-m)E(m)-K(m)K(1-m)=\frac\pi2
\end{equation*}

\section{Solving general elliptic integrals}

Symbolic integrators often give long-winded results for elliptic integrals, mainly because they attack the whole integral at once rather than progressively breaking off small chunks with tidy solutions.

Some of these chunks can be obtained \textit{without} the theory of elliptic integrals.

\subsection{Polynomial reductions}

Let $A,B,C,D,P$ be \textit{arbitrary} univariate polynomials. Rationalising the denominator gives (BF 200.01)
\begin{equation*}
\int\frac{A+B\sqrt P}{C+D\sqrt P}\,dx=\int\frac{AC-BDP}{C^2-D^2P}\,dx+\int\frac{(BC-AD)P}{C^2-D^2P}\cdot\frac1{\sqrt P}\,dx
\end{equation*}

The first integral is elementary. The second integral can be simplified with only GCD operations using Hermite reduction, detailed in Labahn and Mutrie's \textit{Reduction of Elliptic Integrals to Legendre Normal Form}.\footnote{\url{https://cs.uwaterloo.ca/~glabahn/Papers/ellipticPrePrint.pdf}} Suppose $C^2-D^2P$ has square-free factorisation $\prod_i(c_id_i)^i$ where the $c_i$ are coprime to $P$ and the $d_i$ are divisors of $P$, then the second integral is a sum of integrals of the forms
\begin{equation*}
\int\frac a{c^n\sqrt P}\,dx\qquad\text{and}\qquad\int\frac b{d^n\sqrt P}\,dx
\end{equation*}
where $\deg a<\deg c$ and $\deg b<\deg d$. These integrals reduce via the recurrences
\begin{equation*}
\int\frac a{c^n\sqrt P}\,dx=\frac{-v\sqrt P}{(n-1)c^{n-1}}+\int\frac{u+(v'P+vP'/2)/(n-1)}{c^{n-1}\sqrt P}\,dx
\end{equation*}
where $u$ and $v$ solve $cu+Pc'v=a$ and
\begin{equation*}
\int\frac b{d^n\sqrt P}\,dx=\frac{v\sqrt P}{d^n}+\int\frac{u-v'P/d}{d^{n-1}\sqrt P}\,dx
\end{equation*}
where $u$ and $v$ solve $du+(P'/2-nd'P/d)v=b$. When $c$ and $d$ are linear these recurrences reduce to BF 230.02, .03, 250.02 and .03.

In the end, Hermite reduction produces the following decomposition:
\begin{equation*}
\int\frac{(BC-AD)P}{C^2-D^2P}\cdot\frac1{\sqrt P}\,dx=R_1\sqrt P+\int\frac Q{\sqrt P}\,dx+\int\frac{R_2}{\sqrt P}\,dx
\end{equation*}
where $R_1$ is some rational function, $Q$ is a polynomial and $R_2$ is a proper rational function with squarefree denominator. The integral containing $Q$ can itself be reduced with another recurrence: let $I_k=\int\frac{x^k}{\sqrt P}\,dx$, $P(x)=\sum_ic_ix^i$ and $\deg P=n$, then as long as $2(m+1)\ne n$
\begin{equation*}
I_m=\frac1{c_n(m-n/2+1)}\left(x^{m-n+1}\sqrt P-\sum_{k=0}^{n-1}c_k(m-n+1+k/2)I_{m-n+k}\right)
\end{equation*}

The cubic and quartic cases of this recurrence are BF 230.01 and 250.01 respectively. As for the integral containing $R_2$, it can be broken up into partial fractions easily and \textit{implicitly} (no factorisation required):
\begin{equation*}
R_2=\frac{P_n}{P_d}=\sum_{\alpha:P_d(\alpha)=0}\frac{(P_n/P_d'\bmod P_d)(\alpha)}{x-\alpha}
\end{equation*}

For general $P$, the three reductions described above reduce the integral of a rational function of $x$ and $P$ to elementary integrals and \textit{basic hyperelliptic integrals}:
\begin{equation*}
I_k=\int\frac{x^k}{\sqrt P}\,dx\quad(0\le k\le\deg P-2)\qquad\text{and}\qquad I_{(a)}=\int\frac1{(x-a)\sqrt P}\,dx
\end{equation*}

\subsection{Odd power elimination for quartics}

The Byrd and Friedman tables are complete. In particular, they provide solutions for integrals where the polynomial under root $P$ is a generic quartic with all roots complex (BF 267.00 to .05 and .50). However, those solutions are extremely complicated; the fixed bound is not a simple function of roots, for example. The solutions provided when $P$ has two real and two complex roots are also not very simple.

In cases like these, it may be desirable to eliminate $P$'s odd-degree terms and obtain a biquadratic, which is usually easier to solve than a cubic. This can be done using a linear fractional substitution. Suppose $P=a(x^2-s_1x+p_1)(x^2-s_2x+p_2)$, then the substitution is
\begin{equation*}
x=\begin{cases}
\frac{ku+l}{u+1},k+l=\frac{2(p_1-p_2)}{s_1-s_2},kl=\frac{p_1s_2-p_2s_1}{s_1-s_2}&s_1\ne s_2\\u+\frac{s_1}2&s_1=s_2
\end{cases}
\end{equation*}

This substitution should be performed before any polynomial reduction because the $s_1\ne s_2$ case introduces factors of $u+1$ in the denominator.

Now suppose $P=c_4x^4+c_2x^2+c_0$ is biquadratic. After polynomial reductions, $I_1$ in this case is elementary by substituting $u=x^2$:
\begin{equation*}
\int\frac x{\sqrt P}\,dx=\frac12\int\frac1{\sqrt{c_4u^2+c_2u+c_0}}\,du
\end{equation*}

Through the same substitution, $I_{(a)}$ can have an elementary part split off:
\begin{equation*}
\int\frac1{(x-a)\sqrt P}\,dx=\int\frac{x+a}{(x^2-a^2)\sqrt P}\,dx=\frac12\int\frac1{(u-a^2)\sqrt {c_4u^2+c_2u+c_0}}\,du+a\int\frac1{(x^2-a^2)\sqrt P}\,dx
\end{equation*}

This decomposition remains valid if a pole introduced at $-a$ is on the integration path, but the ``infinite parts'' of the elementary and elliptic sides must then be carefully cancelled out. In practice, the extra work means this is not widely used.

\subsection{Sixteen elliptic cases}

Each section of Byrd and Friedman's integral tables ends with a most general case of the form
\begin{equation*}
\int_p^q\frac R{\sqrt P}\,dx=g\int_0^{F(\varphi,m)}R(f(u))\,du
\end{equation*}
where $R$ is an arbitrary rational function of $x$, $f$ is some rational combination of the Jacobi elliptic functions and $g,\varphi,m,f$ depend only on $P$ and the integration bounds. Polynomial reductions allow restricting $R$ to $1$, $x$, $x^2$ and $\frac1{x-a}$, minimising the work needed for solving the second integral.

Sixteen of these cases are presented below, all with fixed finite lower limit $p$ and variable upper limit $q$. Integrals with other forms of $P$ and other bounds can be obtained through odd power elimination and by subtracting two integrals with the same integrands but overlapping bounds.

\begin{center}
$P=(x-a)(x-b)(x-c)$, $a>b>c$
	
\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$c$&$\frac2{\sqrt{a-c}}$&$\sin^{-1}\sqrt{\frac{q-c}{b-c}}$&$\frac{b-c}{a-c}$&$c-(c-b)\sn^2u$&233.20\\
	$a$&$\frac2{\sqrt{a-c}}$&$\sin^{-1}\sqrt{\frac{q-a}{q-b}}$&$\frac{b-c}{a-c}$&$\frac{a-b\sn^2u}{\cn^2u}$&237.20\\
\end{tabular}
	
$P=-(x-a)(x-b)(x-c)$, $a>b>c$
	
\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$b$&$\frac2{\sqrt{a-c}}$&$\sin^{-1}\sqrt{\frac{(a-c)(q-b)}{(a-b)(q-c)}}$&$\frac{a-b}{a-c}$&$\frac{b-cm\sn^2u}{\dn^2u}$&235.20
\end{tabular}

$P=(x-a)((x-b)^2+c)$, $c>0$, $A=\sqrt{(b-a)^2+c}$

\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$a$&$\frac1{\sqrt A}$&$\cos^{-1}\frac{A+a-q}{A-a+q}$&$\frac12+\frac{b-a}{2A}$&$\frac{a+A+(a-A)\cn u}{1+\cn u}$&239.08
\end{tabular}

$P=(x^2-a^2)(x^2-b^2)$, $a>b>0$

\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$0$&$\frac1a$&$\sin^{-1}\frac qb$&$\frac{b^2}{a^2}$&$b\sn u$&219.15\\
	$a$&$\frac1a$&$\sin^{-1}\sqrt{\frac {q^2-a^2}{q^2-b^2}}$&$\frac{b^2}{a^2}$&$a\dc u$&216.14
\end{tabular}

$P=-(x^2-a^2)(x^2-b^2)$, $a>b>0$

\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$-a$&$\frac1a$&$\sin^{-1}\sqrt{\frac{a^2-q^2}{a^2-b^2}}$&$1-\frac{b^2}{a^2}$&$-a\dn u$&218.15\\
	$b$&$\frac1a$&$\sin^{-1}\left(\frac aq \sqrt{\frac{q^2-b^2}{a^2-b^2}}\right)$&$1-\frac{b^2}{a^2}$&$b\nd u$&217.14
\end{tabular}

$P=(x^2-a^2)(x^2+b^2)$, $a>0,b>0$

\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$a$&$\frac1{\sqrt{a^2+b^2}}$&$\cos^{-1}\frac aq$&$\frac{b^2}{a^2+b^2}$&$a\nc u$&211.16
\end{tabular}

$P=-(x^2-a^2)(x^2+b^2)$, $a>0,b>0$

\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$0$&$\frac1{\sqrt{a^2+b^2}}$&$\sin^{-1}\frac qa\sqrt{\frac{a^2+b^2}{q^2+b^2}}$&$\frac{a^2}{a^2+b^2}$&$b\sqrt m\sd u$&214.15
\end{tabular}

$P=(x^2+a^2)(x^2+b^2)$, $a>b>0$

\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$0$&$\frac1a$&$\tan^{-1}\frac qb$&$1-\frac{b^2}{a^2}$&$b\tn u$&221.15
\end{tabular}

$P=(x^2+z^2)(x^2+\overline z^2)$, $z=a+bi$, $a\ne0,b\ne0$

$t$ is the smaller root of $b^2t^2-(q^2+|z|^2)t+q^2=0$ ($q=\infty\implies t=1$)

\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$0$&$\frac1{|z|}$&$\sin^{-1}\sqrt t$&$\frac{b^2}{a^2+b^2}$&$|z|\tn u\dn u$&225.09\\
	$0$&$\frac1{2|z|}$&$2\tan^{-1}\frac q{|z|}$&$\frac{b^2}{a^2+b^2}$&$|z|\sqrt{\frac{1-\cn u}{1+\cn u}}$&225.09
\end{tabular}

$P=(x-a)(x-b)(x-c)(x-d)$, $a>b>c>d$

\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$c$&$\frac2{\sqrt{(a-c)(b-d)}}$&$\sin^{-1}\sqrt{\frac{(b-d)(q-c)}{(b-c)(q-d)}}$&$\frac{(b-c)(a-d)}{(a-c)(b-d)}$&$\frac{c-dn\sn^2u}{1-n\sn^2u}\ \left(n=\frac{b-c}{b-d}\right)$&254.41\\
	$a$&$\frac2{\sqrt{(a-c)(b-d)}}$&$\sin^{-1}\sqrt{\frac{(b-d)(q-a)}{(a-d)(q-b)}}$&$\frac{(b-c)(a-d)}{(a-c)(b-d)}$&$\frac{a-bn\sn^2u}{1-n\sn^2u}\ \left(n=\frac{a-d}{b-d}\right)$&258.41
\end{tabular}

$P=-(x-a)(x-b)(x-c)(x-d)$, $a>b>c>d$

\begin{tabular}{cccccc}
	$p$&$g$&$\varphi$&$m$&$f$&BF\\
	\hline
	$d$&$\frac2{\sqrt{(a-c)(b-d)}}$&$\sin^{-1}\sqrt{\frac{(a-c)(q-d)}{(c-d)(a-q)}}$&$\frac{(a-b)(c-d)}{(a-c)(b-d)}$&$\frac{d-an\sn^2u}{1-n\sn^2u}\ \left(n=\frac{d-c}{a-c}\right)$&252.41\\
	$b$&$\frac2{\sqrt{(a-c)(b-d)}}$&$\sin^{-1}\sqrt{\frac{(a-c)(q-b)}{(a-b)(q-c)}}$&$\frac{(a-b)(c-d)}{(a-c)(b-d)}$&$\frac{b-cn\sn^2u}{1-n\sn^2u}\ \left(n=\frac{a-b}{a-c}\right)$&256.41
\end{tabular}
\end{center}

The second formula in the $P=(x^2+z^2)(x^2+\overline z^2)$ case should only be used when $R(t)$ is a rational function of $t^2$, as that way the second integral has no square root.

\subsection{Solving the second integral}

The sixteen cases above do not solve an elliptic integral directly; they transform it into the integral of a rational combination of the Jacobi elliptic functions. Using the identities $\sn^2u+\cn^2u=m\sn^2u+\dn^2u=1$ and rationalising the denominator when it appears, any such function can be decomposed as (BF 364.01)
\begin{equation*}
R(\sn u,\cn u,\dn u)=R_1(\sn u)+R_2(\sn u)\cn u+R_3(\sn u)\dn u+R_4(\sn u)\cn u\dn u
\end{equation*}
where $R_1,R_2,R_3,R_4$ are more rational functions. Integrals of the last three parts reduce to purely rational integrals through substitutions (BF 364.02, .03, .04):
\begin{equation*}
\int R(\sn u)\cn u\,du=2\int R\left(\frac{2t}{1+mt^2}\right)\cdot\frac1{1+mt^2}\,dt\qquad\left(t=\frac{\sn u}{1+\dn u}\right)\\
\end{equation*}
\begin{equation*}
\int R(\sn u)\dn u\,du=2\int R\left(\frac{2t}{1+t^2}\right)\cdot\frac1{1+t^2}\,dt\qquad\left(t=\frac{\sn u}{1+\cn u}\right)
\end{equation*}
\begin{equation*}
\int R(\sn u)\cn u\dn u\,du=\int R(t)\,dt\qquad(t=\sn u)
\end{equation*}

This leaves only $\int R(\sn u)\,du$. Define for arbitrary $\varphi$ and $m$
\begin{equation*}
F=F(\varphi,m)=u^*\qquad\text{and}\qquad E=E(\varphi,m)
\end{equation*}

Then partial fractions and the following recurrences finally finish the solution to the general elliptic integral:
\begin{align*}
A_k=\int_0^{u^*}\sn^k u\,du\implies&A_0=F\\
&A_1=\frac{\log(\dn u^*-\sqrt m\cn u^*)}{\sqrt m}\\
&A_2=\frac{F-E}m\\
k\ge3\implies&A_k=\frac{(k-2)(1+m)A_{k-2}-(k-3)A_{k-4}+\sn^{k-3}u^*\cn u^*\dn u^*}{(k-1)m}\\
&\text{(BF 310)}
\end{align*}
\begin{align*}
B_k=\int_0^{u^*}\ns^ku\,du\implies&B_0=F\\
&B_1=\log\frac{\sn u^*}{\cn u^*+\dn u^*}\\
&B_2=F-E-\dn u^*\nt u^*\\
k\ge3\implies&B_k=\frac{(k-2)(1+m)B_{k-2}-(k-3)mB_{k-4}-\ns^{k-1}u^*\cn u^*\dn u^*}{k-1}\\
&\text{(BF 311)}
\end{align*}
\begin{align*}
\int_0^{u^*}\frac1{1+a\sn u}\,du&=\Pi(a^2,\varphi,m)+\frac a{AB}\tanh^{-1}\frac{AB(\cn u^*-\dn u^*)}{A^2\cn u^*-B^2\dn u^*}\\
&[a^2\ne1,a^2\ne m,A=\sqrt{a^2-m},B=\sqrt{a^2-1}]\\
\int_0^{u^*}\frac1{1\pm\sqrt m\sn u}\,du&=\frac{E+\sqrt m((1\mp\sqrt m\sn u^*)\cd u^*-1)}{1-m}\\
&\text{(BF 361.52)}\\
\int_0^{u^*}\frac1{1\pm\sn u}\,du&=F-\frac1{1-m}\left(E\pm\left(\frac{\cn u^*\dn u^*}{1+\sn u^*}-1\right)\right)\\
&\text{(BF 361.50)}
\end{align*}
\begin{align*}
C_k&=\int_0^{u^*}(a+\sn u)^{-k}\,du=\frac1{(k-1)(1-a^2)(1-ma^2)}\left(\frac1{a^{k-1}}-\frac{\cn u^*\dn u^*}{(a+\sn u^*)^{k-1}}+\sum_{j=1}^4L_jC_{k-j}\right)\\
&[a^2\ne1,ma^2\ne1,k>1]\\
&[L_1=a(2k-3)(2a^2m-m-1),L_2=-(k-2)(6a^2m-m-1),L_3=2am(2k-5),L_4=-m(k-3)]\\
&=\frac1{a(2k-1)(2a^2m-m-1)}\left(\frac{\cn u^*\dn u^*}{(a+\sn u^*)^k}-\frac1{a^k}+\sum_{j=1}^3L_jC_{k-j}\right)\\
&[a^2=1\lor ma^2=1,L_1=(k-1)(6a^2m-m-1),L_2=-2am(2k-3),L_3=m(k-2)]\\
C_1&=\frac1a\int_0^{u^*}\frac1{1+(1/a)\sn u}\,du
\end{align*}

\subsection{Trigonometric and hyperbolic integrands}

Let $R$ be an arbitrary rational function and $P$ a polynomial with degree at most \textbf{two}. Then the integrals
\begin{equation*}
\int R(\sin\theta,\cos\theta,\sqrt{P(\sin\theta,\cos \theta)})\,d\theta\qquad\text{and}\qquad\int R(\sinh t,\cosh t,\sqrt{P(\sinh t,\cosh t)})\,dt
\end{equation*}
reduce to algebraic elliptic integrals by the substitutions $x=\sin\theta$ and $x=\sinh t$ respectively.

\section{Zhou's ellipsoidal coordinates}

In Zhou Xingling's \textit{Systematic derivations of measures of arbitrary ellipsoids}\footnote{\textit{Journal of Interdisciplinary Mathematics}, \textbf{14} (5-6), pp. 493-506. doi:10.1080/09720502.2011.10700768} the space in the ellipsoid $(x/a)^2+(y/b)^2+(z/c)^2=1$ where $a>b>c>0$ is given the following orthogonal cooordinate system:
\begin{align*}
X&=\frac{(A-r_c)(A-r_b)(A-r_a)}{(A-B)(A-C)}\\
Y&=\frac{(B-r_c)(B-r_b)(r_a-B)}{(A-B)(B-C)}\\
Z&=\frac{(C-r_c)(r_b-C)(r_a-C)}{(A-C)(B-C)}
\end{align*}
where a capital letter denotes the corresponding lowercase variable squared. $(r_a,r_b,r_c)$ represents the orthogonal intersection in any fixed octant of the three surfaces
\begin{align*}
S_1(r_c):\frac X{A-r_c}+\frac Y{B-r_c}+\frac Z{C-r_c}&=1\quad(0<r_c<C)\\
S_2(r_b):\frac X{A-r_b}+\frac Y{B-r_b}-\frac Z{r_b-C}&=1\quad(C<r_b<B)\\
S_3(r_a):\frac X{A-r_a}-\frac Y{r_a-B}-\frac Z{r_a-C}&=1\quad(B<r_a<A)
\end{align*}
which are an ellipsoid, a one-sheet hyperboloid and a two-sheet hyperboloid respectively. When integrating over any coordinate the corresponding scale factor must be included in the integrand:
\begin{align*}
h_c&=\frac{\sqrt{(r_b-r_c)(r_a-r_c)}}{2\sqrt{(A-r_c)(B-r_c)(C-r_c)}}\\
h_b&=\frac{\sqrt{(r_b-r_c)(r_a-r_b)}}{2\sqrt{(A-r_b)(B-r_b)(r_b-C)}}\\
h_a&=\frac{\sqrt{(r_a-r_c)(r_a-r_b)}}{2\sqrt{(A-r_a)(r_a-B)(r_a-C)}}
\end{align*}

The claimed advantages of this specific formulation of ellipsoidal coordinates are the positivity of all factors and the close correspondence between the obtained integrals and the integrals listed in tables like Byrd and Friedman. For example, to compute the surface area of the underlying ellipsoid, set $r_c=0$, then
\begin{align*}
S&=8\int_B^A\int_C^Bh_ah_b\,dr_b\,dr_a\\
&=2\int_B^A\int_C^B\frac{\sqrt{r_ar_b}(r_a-r_b)}{\sqrt{(A-r_a)(r_a-B)(r_a-C)}\cdot\sqrt{(A-r_b)(B-r_b)(r_b-C)}}\,dr_b\,dr_a\\
&=2(I_a(2)I_b(1)-I_a(1)I_b(2))
\end{align*}
where
\begin{equation*}
I_a(n)=\int_B^A\frac{r_a^n}{\sqrt{(A-r_a)(r_a-B)(r_a-C)r_a}}\,dr_a\qquad\text{and}\qquad I_b(n)=\int_C^B\frac{r_b^n}{\sqrt{(A-r_b)(B-r_b)(r_b-C)r_b}}\,dr_b
\end{equation*}

Solving the $I_a(n)$ and $I_b(n)$ then gives
\begin{equation*}
S=4\left(E(1-m)-\frac{AB}{g^2}K(1-m)\right)C\Pi(n_2,m)-4\left(E(m)+\frac{BC}{g^2}K(m)\right)(CK(1-m)+(B-C)\Pi(n_1,1-m))
\end{equation*}
where
\begin{equation*}
g=\sqrt{(A-C)B}\qquad m=\frac{(B-C)A}{(A-C)B}\qquad n_1=\frac{A-B}{A-C}\qquad n_2=\frac{B-C}B
\end{equation*}
Reducing the complete third-kind integrals to incomplete first- and second-kind integrals (BF 413.01, 414.01), then applying Legendre's relation, simplifies this to
\begin{align*}
S&=4(C+E(\varphi,m)g+BCF(\varphi,m)/g)(K(m)E(1-m)+K(1-m)E(m)-K(m)K(1-m))\\
&=2\pi(C+E(\varphi,m)g+BCF(\varphi,m)/g)
\end{align*}
where $\varphi=\cos^{-1}\frac ca$. Similarly, the integral of mean curvature $\frac1{4h_c}\left(\frac1{r_a}+\frac1{r_b}\right)$ over the underlying ellipsoid is the surface area of the ellipsoid with semi-axes $\sqrt{ab/c},\sqrt{ca/b},\sqrt{bc/a}$ and the integral of squared mean curvature (Willmore energy) is
\begin{equation*}
\frac\pi{3ABC}\Big(C(5AB+2AC+2BC)+2(AB+AC+BC)E(\varphi,m)g+BC(AB+AC+2BC-A^2)F(\varphi,m)/g\Big)
\end{equation*}
\end{document}