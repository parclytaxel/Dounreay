matfrompol(p) = {
    m = poldegree(p, x);
    n = poldegree(p, y);
    return(matrix(n+1,m+1,i,j,polcoef(polcoef(p,i-1,y),j-1,x)));
}

/* following http://www.math.clemson.edu/~sgao/papers/fac_bipoly.pdf
S. Gao (2003), Factoring multivariate polynomials via
partial differential equations, Math. Comp. 72 (242), 801-822 */

polgaosf(f) = {
    \\ assume f is squarefree
    F = matfrompol(f);
    [n,m] = matsize(F) - [1,1];
    G = matrix(4 * m * n, 2 * m * n + m + n);
    ho = m * (n + 1);
    forvec(fgy_fhx = [[1,n+1],[1,m+1],[1,n],[1,m]],
        [i,j,k,l] = fgy_fhx;
        a = i + k - 1;
        b = j + l - 1;
        zi = (a - 1) * 2 * m + b;
        gi = k * m + l;
        G[zi,gi] += k * F[i,j];
        hi = (k - 1) * (m + 1) + (l + 1);
        G[zi,hi+ho] -= l * F[i,j];
    );
    forvec(hfx = [[1,n+1], [1,m], [1,n], [1,m+1]],
        [i,j,k,l] = hfx;
        a = i + k - 1;
        b = j + l - 1;
        zi = (a - 1) * 2 * m + b;
        hi = (k - 1) * (m + 1) + l;
        G[zi,hi+ho] += j * F[i,j+1];
    );
    forvec(gfy = [[1,n], [1,m+1], [1,n+1], [1,m]],
        [i,j,k,l] = gfy;
        a = i + k - 1;
        b = j + l - 1;
        zi = (a - 1) * 2 * m + b;
        gi = (k - 1) * m + l;
        G[zi,gi] -= i * F[i+1,j];
    );
    
    printf("G @ %d\n", matsize(G));
    
    k = lindep(G)[1..m*(n+1)];
    kc = vector(m*(n+1),i,x^((i-1)%m)*y^((i-1)\m));
    \\ Jürgen Gerhard's improvement to the algorithm
    g = kc * k;
    fx = deriv(f, x);
    zp = content(polresultant(f, g - z * fx), y);
    return(factor(zp)[,1]~);
}

addhelp(polgaosf, "polgaosf(f): field extension over which f(x,y) splits using Gao's algorithm, assuming gcd(f,deriv(f,x))==1.");
